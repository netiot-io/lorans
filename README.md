**What does the microservice**
-------------------------

It receives over MQTT encrypted data form a LoRa antenna and decrypts is it to be used by the application. The resulted plain text data send to be transformed by the decoder service as it is binary data.
The solution is split between in several projects from which result two binaries, one is the Network Server and the other is a bridge that received UDP packets from the LoRa antenna and forwards them over MQTT to the Network Server in the cloud.

-------------------------
**Compilation**

Required:

	* gcc and g++ version >= 8.2 for local build and optional Visual Studio with the Linux development package for remote build, the solution uses the C++17 standard
	* [libcryptopp](https://github.com/weidai11/cryptopp/)
	* [librdkafka](https://github.com/edenhill/librdkafka)
	* mosquitto-dev
	* postgresql-dev
	* make

To build locally the project go into the root directory and execute `make release all` to build both executables or use `PacketBridge` or `NetworkServer` instead of `all` to build each executable individualy.
To clean the solution use `make clean`.


-------------------------
**Environment variables**

| **Variable**                   | **Description**                                                                       | **Used in**                   |
|--------------------------------|---------------------------------------------------------------------------------------|-------------------------------|
| MQTT_BROKER_ADDRESS            | MQTT broker address                                                                   | Network Server, Packet Bridge |
| MQTT_BROKER_PORT               | MQTT broker port                                                                      | Network Server, Packet Bridge |
| MQTT_CLIENT_ID                 | MQTT client ID                                                                        | Network Server, Packet Bridge |
| MQTT_VERSION                   | MQTT version (supported only 3.1 and 3.1.1)                                           | Network Server, Packet Bridge |
| MQTT_USE_USERNAME_AND_PASSWORD | Should be `true` if the MQTT broker requires username and password, `false` otherwise | Network Server, Packet Bridge |
| MQTT_SEND_TOPIC                | The MQTT topic on which to send data                                                  | Packet Bridge                 |
| MQTT_RECEIVE_TOPIC             | The MQTT topic on which to receive data                                               | Network Server                |
| MQTT_QOS                       | The quality of service to use for the MQTT connection (0, 1 or 2)                     | Network Server, Packet Bridge |
| KAFKA_BROKER_LIST              | Kafka node list, format <host1_address>:<host1_port>,<host2_address>:<host2_port>     | Network Server                |
| KAFKA_CONSUMER_GROUP_ID        | The Kafka consumer group ID                                                           | Network Server                |
| KAFKA_PRODUCE_TOPIC            | The Kafka topic on which to send data                                                 | Network Server                |
| KAFKA_CONSUME_TOPIC            | The Kafka topic on which to receive data                                              | Network Server                |
| DATABASE_ADDRESS               | The PostgreSQL database address                                                       | Network Server                |
| DATABASE_PORT                  | The PostgreSQL database port                                                          | Network Server                |
| DATABASE_USER                  | The PostgreSQL database username                                                      | Network Server                |
| DATABASE_PASSWORD              | The PostgreSQL database password                                                      | Network Server                |
| DATABASE_NAME                  | The PostgreSQL database name                                                          | Network Server                |

