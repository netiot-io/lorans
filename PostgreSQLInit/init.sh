#!/bin/bash

set -e
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    create schema ns_schema;
    create table ns_schema.gateway_list (gw_mac text primary key, model text, last_contact int8);
    create table ns_schema.gateway_status (gw_mac text references ns_schema.gateway_list(gw_mac), time text, lati numeric, long numeric, alti numeric, rxnb int4, rxok int4, rxfw int4, ackr int4, dwnb int4, txnb int4);
    create table ns_schema.devices (id int8 primary key, active boolean, deveui text, activation_method text not null, last_contact int8, appeui text, appkey text, devaddr text, nwskey text, appskey text, fcntup int4, fcntdown int4, unique(deveui));
    create table ns_schema.rfpackets (id bigserial primary key, deveui text references ns_schema.devices(deveui) on delete cascade on update cascade, received_time int8, time text, tmms int8, tmst int8, freq numeric, chan numeric, rfch int4, stat int4, modu text, datr text, codr text, rssi int4, lsnr numeric, size int4, data text);
EOSQL
