/*
 * MQTTClient.cpp
 *
 *  Created on: Mar 13, 2018
 *      Author: Say10
 */


#include "MQTTClient.hpp"
#include <iostream>
#include <thread>
#include <cstring>

MQTTClient::MQTTClient(void) {}

MQTTClient::~MQTTClient(void) {}

void MQTTClient::on_connect(int rc)
{
	if (rc != MOSQ_ERR_SUCCESS) {
		error = true;
		std::cerr << "\nAn error occurred while connecting!\n";
	} else
		std::cerr << "\nThe client has connected to the broker!\n";

	connected = true;
}

void MQTTClient::on_disconnect(int rc)
{
	std::cerr << "\nThe client has disconnected from the broker with error code: " << std::string(mosqpp::strerror(rc)) << "!\n";

	if (rc != MOSQ_ERR_SUCCESS)
		error = true;

	connected = false;
}

void MQTTClient::on_publish(int mid)
{
	std::cerr << "\nPublished message with ID " << mid << "!\n";
}

void MQTTClient::on_message(const struct mosquitto_message *message)
{
	std::cerr << "\nReceived message!\n";

	submessage smsg;

	smsg.topic = std::string(message->topic);
	smsg.message = std::vector<unsigned char>((unsigned char *) message->payload, (unsigned char *) message->payload + message->payloadlen);

	received_messages.push(smsg);
}

void MQTTClient::on_subscribe(int mid, int qos_count, const int *granted_qos)
{

}

void MQTTClient::on_unsubscribe(int mid)
{

}

void MQTTClient::on_log(int level, const char *str)
{

}

void MQTTClient::on_error(void)
{
	std::cerr << "\nAn error occurred!\n";

	error = true;
}

void MQTTClient::reset_error(void)
{
	error = false;
}

bool MQTTClient::error_occurred(void)
{
	return error;
}

size_t MQTTClient::get_message_count(void)
{
	return received_messages.size();
}

submessage MQTTClient::get_message(void)
{
	submessage message;

	if (received_messages.size() > 0) {
		message = received_messages.front();

		received_messages.pop();

		return message;
	}

	return message;
}

void MQTTClient::try_connection(void)
{
	connect(brokerAddress.c_str(), mqttPort, 15);
	loop(1000);
	loop(1000);
}

bool MQTTClient::is_connected(void)
{
	return connected;
}

void MQTTClient::try_reconnect(void)
{
	reconnect();
	loop(1000);
	loop(1000);
}

void MQTTClient::try_loop(int ms)
{
	if (!connected && !error) {
		try_reconnect();
		loop(5000);

		if (!connected) {
			std::this_thread::sleep_for(std::chrono::seconds(1));

			return;
		}
	}

	loop(ms);

	if (error) {
		disconnect();
		loop(5000);
		reset_error();
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

void MQTTClient::SetUpMQTT(void)
{
	std::string clientID = "Anonymous";
	bool useCAFile = false;
	bool useUsernameAndPasssword = false;
	int MQTTVersion = MQTT_PROTOCOL_V31;
	std::string username = "";
	std::string password = "";
	std::string CAFileLocation = "./none";

	const char *env = NULL;

	if ((env = std::getenv("MQTT_BROKER_ADDRESS")) != NULL)
		brokerAddress = std::string(env);

	if ((env = std::getenv("MQTT_CLIENT_ID")) != NULL)
		clientID = std::string(env);

	if ((env = std::getenv("MQTT_PORT")) != NULL)
		mqttPort = (unsigned short) std::stoul(env, NULL, 10);

	if ((env = std::getenv("MQTT_VERSION")) != NULL && strcmp(env, "3.1.1") == 0)
		MQTTVersion = MQTT_PROTOCOL_V311;

	if ((env = std::getenv("MQTT_USE_USERNAME_AND_PASSWORD")) != NULL &&
			strcmp(env, "false") != 0) {
		if ((env = std::getenv("MQTT_USERNAME")) != NULL) {
			username = std::string(env);

			if ((env = std::getenv("MQTT_PASSWORD")) != NULL) {
				password = std::string(env);
				useUsernameAndPasssword = true;
			}
		}

		if ((env = std::getenv("MQTT_USE_CA_FILE")) != NULL &&
				strcmp(env, "false") != 0 &&
				(env = std::getenv("MQTT_CA_FILE_LOCATION")) != NULL) {
			useCAFile = true;
			CAFileLocation = std::string(env);
		}
	}

	if ((env = std::getenv("MQTT_SEND_TOPIC")) != NULL)
		sendTopic = std::string(env);

	if ((env = std::getenv("MQTT_RECEIVE_TOPIC")) != NULL)
		receiveTopic = std::string(env);

	if ((env = std::getenv("MQTT_QOS")) != NULL)
		recommandedQoS = (int) std::stoul(env, NULL, 10);

	reinitialise(clientID.c_str(), false);
	threaded_set(false);
	opts_set(MOSQ_OPT_PROTOCOL_VERSION, &MQTTVersion);

	if (useUsernameAndPasssword) {
		username_pw_set(username.c_str(), password.c_str());

		if (useCAFile) {
			tls_insecure_set(false);
			tls_set(CAFileLocation.c_str());
		}
	}
}
