
MQTT_CPP_SRCS += \
MQTTClient/MQTTClient.cpp

MQTT_OBJS += \
MQTTClient/MQTTClient.o


MQTT_CPP_DEPS += \
MQTTClient/MQTTClient.d

MQTT_INCL = \
-IParsers \
-IProcessInterface

# Each subdirectory must supply rules for building sources it contributes
MQTTClient/%.o: MQTTClient/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) $(MQTT_INCL) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



