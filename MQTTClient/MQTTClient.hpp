/*
 * MQTTClient.hpp
 *
 *  Created on: Mar 13, 2018
 *      Author: Say10
 */

#ifndef MQTTCLIENT_HPP_
#define MQTTCLIENT_HPP_

#include <mosquittopp.h>
#include <vector>
#include <string>
#include <queue>

struct submessage {
	std::string topic;
	std::vector<unsigned char> message;
};

class MQTTClient : public mosqpp::mosquittopp {
private:
	bool error = false;
	bool connected = false;
	std::queue<submessage> received_messages;

public:
	std::string sendTopic = "SendTopic";
	std::string receiveTopic = "ReceiveTopic";
	unsigned short mqttPort = 1883;
	std::string brokerAddress = "localhost";
	int recommandedQoS = 0;

	MQTTClient(void);
	~MQTTClient(void);

	void on_connect(int rc) override;
	void on_disconnect(int rc) override;
	void on_publish(int mid) override;
	void on_message(const struct mosquitto_message *message) override;
	void on_subscribe(int mid, int qos_count, const int *granted_qos) override;
	void on_unsubscribe(int mid) override;
	void on_log(int level, const char *str) override;
	void on_error(void) override;

	bool error_occurred(void);
	void reset_error(void);

	size_t get_message_count(void);
	submessage get_message(void);
	void try_connection(void);
	bool is_connected(void);
	void try_reconnect(void);
	void try_loop(int ms);

	void SetUpMQTT(void);
};

#endif /* MQTTCLIENT_HPP_ */
