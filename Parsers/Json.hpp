/*
 * Json.hpp
 *
 *  Created on: Mar 22, 2018
 *      Author: say10
 */

#ifndef PARSER_JSON_HPP_
#define PARSER_JSON_HPP_

#include <vector>
#include <map>
#include <string>
#include <fstream>

#include "parson.h"

class Json {
public:
	enum ObjectType {
		JsonObject,
		Array,
		Number,
		Boolean,
		String,
		Null
	};

	class Element {
	private:
		ObjectType type = Null;
		void *object = NULL;

	public:
		Element(void);
		~Element(void);

		Element(const Element &obj);

		Element &operator=(const Element &obj);

		void ClearValue(void);
		void SetValue(Json val);
		void SetValue(std::vector<Element> val);
		void SetValue(double val);
		void SetValue(bool val);
		void SetValue(std::string val);
		void SetValue(const char *val);

		Json GetJson(void);
		std::vector<Element> GetArray(void);
		double GetNumber(void);
		bool GetBoolean(void);
		std::string GetString(void);
		ObjectType GetType(void);
	};

private:

	std::map<std::string, Element> elements;

	static void Copy(JSON_Object *obj, Json &json);
	static void Copy(JSON_Value *value, Element &elem);

public:
	Json(void);
	~Json(void);

	Json(const Json &obj);
	Json &operator=(const Json &obj);

	Json(std::string jsonStr, bool withComments = false);
	Element &operator[](const std::string name);
	bool HasField(const std::string name);
	std::vector<std::string> GetFields(void);

	static void Print(Json json, std::ofstream &out, int count = 0);

	static std::string ToString(Json &json);
	static std::string ToString(Element &elem);
	static Json ReadFromFile(std::string filePath, bool withComments = true);
};


#endif /* PARSER_JSON_HPP_ */
