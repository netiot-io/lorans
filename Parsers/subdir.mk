PARSERS_CPP_SRCS += \
Parsers/Json.cpp

PARSERS_C_SRCS += \
Parsers/parson.c \
Parsers/base64.c

PARSERS_OBJS += \
Parsers/Json.o \
Parsers/parson.o \
Parsers/base64.o

PARSERS_CPP_DEPS += \
Parsers/Json.d

PARSERS_C_DEPS += \
Parsers/parson.d \
Parsers/base64.d


# Each subdirectory must supply rules for building sources it contributes
Parsers/%.o: Parsers/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
	
Parsers/%.o: Parsers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

