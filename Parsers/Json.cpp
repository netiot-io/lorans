/*
 * Json.cpp
 *
 *  Created on: Mar 22, 2018
 *      Author: Say10
 */

#include "Json.hpp"
#include <sstream>

Json::Element::Element(void) {}

Json::Element::~Element(void)
{
	ClearValue();
}

Json::Element::Element(const Json::Element &obj)
{
	*this = obj;
}

Json::Element &Json::Element::operator=(const Json::Element &obj)
{
	ClearValue();

	type = obj.type;

	switch (type) {
	case JsonObject:
		SetValue(*((Json *) obj.object));
		break;
	case Array:
		SetValue(*((std::vector<Element> *) obj.object));
		break;
	case Number:
		SetValue(*((double *) obj.object));
		break;
	case Boolean:
		SetValue(*((bool *) obj.object));
		break;
	case String:
		SetValue(*((std::string *) obj.object));
		break;
	default:
		break;
	}

	return *this;
}

void Json::Element::ClearValue(void)
{
	if (object == NULL)
		return;

	switch (type) {
	case JsonObject:
		delete ((Json *) object);
		break;
	case Array:
		delete ((std::vector<Element> *) object);
		break;
	case Number:
		delete ((double *) object);
		break;
	case Boolean:
		delete ((bool *) object);
		break;
	case String:
		delete ((std::string *) object);
		break;
	default:
		break;
	}

	object = NULL;
}

void Json::Element::SetValue(Json val)
{
	ClearValue();

	type = JsonObject;
	object = new Json(val);
}

void Json::Element::SetValue(std::vector<Json::Element> val)
{
	ClearValue();

	type = Array;
	object = new std::vector<Element>(val);
}

void Json::Element::SetValue(double val)
{
	ClearValue();

	type = Number;
	object = new double(val);
}

void Json::Element::SetValue(bool val)
{
	ClearValue();

	type = Boolean;
	object = new bool(val);
}

void Json::Element::SetValue(std::string val)
{
	ClearValue();

	type = String;
	object = new std::string(val);
}

void Json::Element::SetValue(const char * val)
{
	ClearValue();

	type = String;
	object = new std::string(val);
}

Json Json::Element::GetJson(void)
{
	if (type == JsonObject)
		return *((Json *) object);

	return Json();
}

std::vector<Json::Element> Json::Element::GetArray(void)
{
	if (type == Array)
		return *((std::vector<Json::Element> *) object);

	return std::vector<Json::Element>();
}

double Json::Element::GetNumber(void)
{
	if (type == Number)
		return *((double *) object);

	return 0;
}

bool Json::Element::GetBoolean(void)
{
	if (type == Boolean)
		return *((bool *) object);

	return false;
}

std::string Json::Element::GetString(void)
{
	if (type == String)
		return *((std::string *) object);

	return std::string();
}

Json::ObjectType Json::Element::GetType(void)
{
	return type;
}

Json::Json(void) {}

Json::~Json(void) {}

Json::Json(std::string jsonStr, bool withComments)
{
	JSON_Value *root = NULL;

	if (withComments)
		root = json_parse_string_with_comments(jsonStr.c_str());
	else
		root = json_parse_string(jsonStr.c_str());

	if (root == NULL)
		return;

	JSON_Object *obj = json_value_get_object(root);

	Copy(obj, *this);


	json_value_free(root);
}

void Json::Copy(JSON_Object *obj, Json &json)
{
	size_t count = json_object_get_count(obj);

	for (size_t i = 0; i < count; ++i) {
		std::string name = json_object_get_name(obj, i);
		JSON_Value *value = json_object_get_value(obj, name.c_str());

		Copy(value, json.elements[name]);
	}
}

void Json::Copy(JSON_Value *value, Json::Element &elem)
{
	JSON_Value_Type type = json_value_get_type(value);
	Json child;
	size_t arrayCount;
	JSON_Array *array;
	std::vector<Element> jarray;

	switch (type) {
	case JSONBoolean:
		elem.SetValue((bool) json_value_get_boolean(value));
		break;
	case JSONNumber:
		elem.SetValue(json_value_get_number(value));
		break;
	case JSONString:
		elem.SetValue(std::string(json_value_get_string(value)));
		break;
	case JSONObject:
		Copy(json_value_get_object(value), child);
		elem.SetValue(child);
		break;
	case JSONArray:
		array = json_value_get_array(value);
		arrayCount = json_array_get_count(array);

		for (size_t j = 0; j < arrayCount; ++j) {
			JSON_Value *arrayVal = json_array_get_value(array, j);
			Element arrayElem;
			Copy(arrayVal, arrayElem);

			jarray.push_back(arrayElem);
		}

		elem.SetValue(jarray);
		break;
	default:
		break;
	}
}

Json::Json(const Json &obj)
{
	*this = obj;
}

Json &Json::operator=(const Json &obj)
{
	elements = obj.elements;

	return *this;
}

Json::Element &Json::operator[](const std::string name)
{
	return elements[name];
}

bool Json::HasField(const std::string name)
{
	return elements.find(name) != elements.end();
}

std::vector<std::string> Json::GetFields(void)
{
	std::vector<std::string> fields;

	for (std::pair<const std::string, Element> &elem : elements)
		fields.push_back(elem.first);

	return fields;
}

std::string Json::ToString(Json &json)
{
	std::stringstream ss;

	std::vector<std::string> fields = json.GetFields();
	size_t count = fields.size();

	if (count == 0)
		return "{}";

	ss << "{\"" << fields[0] << "\":" << ToString(json[fields[0]]);

	for (size_t i = 1; i < count; ++i)
		ss << ",\"" << fields[i] << "\":" << ToString(json[fields[i]]);

	ss << '}';

	return ss.str();
}

std::string Json::ToString(Element &elem)
{
	std::stringstream ss;

	size_t arraySize;
	std::vector<Element> array;
	Json child;

	switch (elem.GetType()) {
	case JsonObject:
		child = elem.GetJson();
		ss << ToString(child);
		break;
	case Array:
		array = elem.GetArray();
		arraySize = array.size();

		if (arraySize == 0) {
			ss << "[]";
			break;
		}

		ss << '[' << ToString(array[0]);

		for (size_t i = 1; i < arraySize; ++i)
			ss << ',' << ToString(array[i]);

		ss << ']';
		break;
	case Number:
		ss << elem.GetNumber();
		break;
	case Boolean:
		ss << (elem.GetBoolean() ? "true" : "false");
		break;
	case String:
		ss << '"' << elem.GetString() << '"';
		break;
	case Null:
	default:
		ss << "null";
		break;
	}

	return ss.str();
}

Json Json::ReadFromFile(std::string filePath, bool withComments)
{
	Json json;

	JSON_Value *root = NULL;

	if (withComments)
		root = json_parse_file_with_comments(filePath.c_str());
	else
		root = json_parse_file(filePath.c_str());

	if (root == NULL)
		return json;

	JSON_Object *obj = json_value_get_object(root);

	Copy(obj, json);

	json_value_free(root);

	return json;
}

void Json::Print(Json json, std::ofstream &out, int count)
{
	for (std::string field : json.GetFields()) {
		for (int i = 0; i < count; ++i)
			out << '\t';

		out << field << ':';

		Json::Element elem = json[field];

		switch (elem.GetType()) {
		case Json::JsonObject:
			out << "{\n";
			Print(elem.GetJson(), out, count + 1);
			out << '}';
			break;
		case Json::String:
			out << elem.GetString();
			break;
		case Json::Number:
			out << elem.GetNumber();
			break;
		case Json::Boolean:
			out << (elem.GetBoolean() ? "true" : "false");
			break;
		default:
			break;
		}

		out << '\n';
	}
}
