/*
 * ProcessInterface.hpp
 *
 *  Created on: May 4, 2018
 *      Author: Say10
 */

#ifndef PROCESSINTERFACE_HPP_
#define PROCESSINTERFACE_HPP_

#include <string>

class ProcessInterface {
protected:
	static volatile bool running;
	static void ExitSignalHandler(int signal);

public:
	ProcessInterface(void);
	virtual ~ProcessInterface(void);

	virtual void PreRun(void);
	virtual void Run(void);
	virtual void PostRun(void);

	static void SetupSignalHandlers(void);
};

#endif /* PROCESSINTERFACE_HPP_ */
