/*
 * ProcessInterface.cpp
 *
 *  Created on: May 4, 2018
 *      Author: Say10
 */

#include <iostream>
#include <signal.h>
#include "ProcessInterface.hpp"

volatile bool ProcessInterface::running = false;

ProcessInterface::ProcessInterface(void) {}
ProcessInterface::~ProcessInterface(void) {}

void ProcessInterface::ExitSignalHandler(int signal)
{
	running = false;

	std::string signalStr;

	switch (signal) {
	case SIGUSR1:
		signalStr = "SIGUSR1";
		break;
	case SIGTERM:
		signalStr = "SIGTERM";
		break;
	case SIGINT:
		signalStr = "SIGINT";
		break;
	case SIGTSTP:
		signalStr = "SIGTSTP";
		break;
	case SIGQUIT:
		signalStr = "SIGQUIT";
		break;
	case SIGHUP:
		signalStr = "SIGHUP";
		break;
	default:
		break;
	}

	std::cerr << "\nCaught signal: " << signalStr << '\n';
}

void ProcessInterface::SetupSignalHandlers(void)
{
	if (signal(SIGUSR1, ExitSignalHandler) == SIG_ERR)
		std::cerr << "\nCould not setup signal handler for SIGUSR1!\n";

	if (signal(SIGTERM, ExitSignalHandler) == SIG_ERR)
		std::cerr << "\nCould not setup signal handler for SIGTERM!\n";

	if (signal(SIGINT, ExitSignalHandler) == SIG_ERR)
		std::cerr << "\nCould not setup signal handler for SIGINT!\n";

	if (signal(SIGTSTP, ExitSignalHandler) == SIG_ERR)
		std::cerr << "\nCould not setup signal handler for SIGTSTP!\n";

	if (signal(SIGQUIT, ExitSignalHandler) == SIG_ERR)
		std::cerr << "\nCould not setup signal handler for SIGQUIT!\n";

	if (signal(SIGHUP, ExitSignalHandler) == SIG_ERR)
		std::cerr << "\nCould not setup signal handler for SIGHUP!\n";
}

void ProcessInterface::PreRun(void) {}

void ProcessInterface::Run(void) {}

void ProcessInterface::PostRun(void) {}

