PROCESSINTERFACE_CPP_SRCS += \
ProcessInterface/ProcessInterface.cpp

PROCESSINTERFACE_OBJS += \
ProcessInterface/ProcessInterface.o

PROCESSINTERFACE_CPP_DEPS += \
ProcessInterface/ProcessInterface.d 


# Each subdirectory must supply rules for building sources it contributes
ProcessInterface/%.o: ProcessInterface/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

