/*
 * KafkaClient.cpp
 *
 *  Created on: Jan 12, 2019
 *      Author: say10
 */

#include <iostream>
#include "KafkaClient.hpp"

KafkaClient::KafkaClient(void) {}

KafkaClient::~KafkaClient(void)
{
	CleanUp();
}

void KafkaClient::CleanUp(void)
{
	if (producer != NULL) {
		while (producer->flush(1000) == RdKafka::ERR__TIMED_OUT) {}

		delete producer;
		producer = NULL;
	}

	if (consumer != NULL) {
		consumer->close();

		delete consumer;
		consumer = NULL;
	}

	configured = false;
}

bool KafkaClient::IsConfigured(void)
{
	return configured;
}

void KafkaClient::SetSubscriptions(std::vector<std::string> topics)
{
	if (!configured) {
		std::cerr << "The Kafka consumer is not initialized!\n";

		return;
	}

    RdKafka::ErrorCode errCode = consumer->subscribe(topics);

    if (errCode != RdKafka::ERR_NO_ERROR)
    	 std::cerr << "Failed to subscribe to topics with error " << RdKafka::err2str(errCode) << '\n';
}

void KafkaClient::SetUpKafkaClient(void)
{
	CleanUp();

    std::string brokers;
    std::string errstr;
    std::string groupID;

    const char *env = NULL;

    if ((env = getenv("KAFKA_BROKER_LIST")) != NULL)
		brokers = std::string(env);
	else
		return;

	if ((env = getenv("KAFKA_CONSUMER_GROUP_ID")) != NULL)
		groupID = std::string(env);
	else
		return;

	if ((env = getenv("KAFKA_PRODUCE_TOPIC")) != NULL)
		produceTopic = std::string(env);
	else
		return;

	if ((env = getenv("KAFKA_CONSUME_TOPIC")) != NULL)
		consumeTopic = std::string(env);
	else
		return;

    RdKafka::Conf *kafkaConf = RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL);

    if (kafkaConf->set("metadata.broker.list", brokers, errstr) != RdKafka::Conf::CONF_OK) {
    	std::cerr << "Setting metadata.broker.list failed: " << errstr << '\n';
        delete kafkaConf;

        return;
    }

    producer = RdKafka::Producer::create(kafkaConf, errstr);

    if (producer == NULL) {
        std::cerr << "Failed to create Kafka producer: " << errstr << '\n';
        delete kafkaConf;

        return;
    }

    if (kafkaConf->set("group.id", groupID, errstr) != RdKafka::Conf::CONF_OK) {
    	std::cerr << "Setting group.id failed: " << errstr << '\n';
        delete kafkaConf;

        return;
    }

    consumer = RdKafka::KafkaConsumer::create(kafkaConf, errstr);

    delete kafkaConf;

    if (consumer == NULL) {
        std::cerr << "Failed to create Kafka consumer: " << errstr << '\n';

        return;
    }

    configured = true;
}

void KafkaClient::Produce(std::string produceTopic, std::string key, std::string value)
{
	if (!configured)
		return;

	producer->produce(produceTopic, 0, RdKafka::Producer::RK_MSG_COPY,
						(char *) value.c_str(), value.size(),
						key.c_str(), key.size(), 0, NULL, NULL);
	producer->poll(0);
}

void KafkaClient::Produce(std::string produceTopic, std::string value)
{
	if (!configured)
		return;

	producer->produce(produceTopic, 0, RdKafka::Producer::RK_MSG_COPY,
						(char *) value.c_str(), value.size(),
						NULL, 0, 0, NULL, NULL);
	producer->poll(0);
}

void KafkaClient::Pool(int millisecs)
{
	if (!configured)
		return;

	producer->poll(millisecs);
}

std::string KafkaClient::ProduceTopic(void)
{
	return produceTopic;
}

std::string KafkaClient::ConsumeTopic(void)
{
	return consumeTopic;
}

std::map<std::string, std::list<std::string>> KafkaClient::GetMessages(void)
{
	std::map<std::string, std::list<std::string>> messages;

	if (!configured) {
		return messages;
	}

	while (true) {
		RdKafka::Message *message = consumer->consume(0);

		if (message->err() == RdKafka::ERR__TIMED_OUT) {
			delete message;

			break;
		} else if (message->err() == RdKafka::ERR_NO_ERROR) {
			std::string topic = message->topic_name();
			std::string value((char *) message->payload(), ((char *) message->payload()) + message->len());

			messages[topic].push_back(value);
		}

		delete message;
	}

	return messages;
}

