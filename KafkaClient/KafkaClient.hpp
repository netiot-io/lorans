/*
 * KafkaClient.hpp
 *
 *  Created on: Jan 12, 2019
 *      Author: say10
 */

#ifndef KAFKACLIENT_HPP_
#define KAFKACLIENT_HPP_

#include <librdkafka/rdkafkacpp.h>
#include <map>
#include <list>

class KafkaClient {
private:
    RdKafka::Producer *producer = NULL;
    RdKafka::KafkaConsumer *consumer = NULL;
	std::string produceTopic;
	std::string consumeTopic;
    bool configured = false;

    void CleanUp(void);

public:
	KafkaClient(void);
	~KafkaClient(void);

	void SetUpKafkaClient(void);
    bool IsConfigured(void);

    void SetSubscriptions(std::vector<std::string> topics);

    void Produce(std::string produceTopic, std::string key, std::string value);
    void Produce(std::string produceTopic, std::string value);
    void Pool(int millisecs);

	std::string ProduceTopic(void);
	std::string ConsumeTopic(void);

    std::map<std::string, std::list<std::string>> GetMessages(void);
};



#endif /* KAFKACLIENT_HPP_ */
