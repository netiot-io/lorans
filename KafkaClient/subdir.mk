
KAFKA_CPP_SRCS += \
KAFKA/KafkaClient.cpp 

KAFKA_OBJS += \
KafkaClient/KafkaClient.o


KAFKA_CPP_DEPS += \
KafkaClient/KafkaClient.d

KAFKA_INCL = \
-IParsers \
-IProcessInterface

# Each subdirectory must supply rules for building sources it contributes
KafkaClient/%.o: KafkaClient/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) $(KAFKA_INCL) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



