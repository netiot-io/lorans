RELDIR = bin/Release
DEBUGDIR = bin/Debug
BUILDLOG = build.log

ifeq "$(findstring release, $(MAKECMDGOALS))" ""
	BUILD=$(DEBUGDIR)
	CCCALLFLAGS += -g3 -O0 -D_DEBUG -D_GLIBCXX_DEBUG -Wall -Wno-unknown-pragmas -Wno-format -c
else
	BUILD=$(RELDIR)
	CCCALLFLAGS += -O3 -c
endif

CC = g++ 
STD = -std=c++17

export

include Parsers/subdir.mk
include PostgreSQL/subdir.mk
include MQTTClient/subdir.mk
include KafkaClient/subdir.mk
include PacketBridge/subdir.mk
include NetworkServer/subdir.mk
include ProcessInterface/subdir.mk

all: PacketBridge NetworkServer

config:
	@echo 'Creating directory bin'
	mkdir -p bin
	mkdir -p $(RELDIR)
	mkdir -p $(DEBUGDIR)

release:
	@echo 'Finished creating release build'

# Tool invocations

PacketBridge: $(PCKBR_OBJS) $(PROCESSINTERFACE_OBJS) $(MQTT_OBJS) $(PARSERS_OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: GCC C++ Linker'
	$(CC) $(STD) $(PCKBR_OBJS) $(PROCESSINTERFACE_OBJS) $(MQTT_OBJS) $(PARSERS_OBJS) -o $(BUILD)/PacketBridge -lmosquittopp -lstdc++fs
	@echo 'Finished building target: $@'
	@echo ' '

NetworkServer: $(NS_OBJS) $(PROCESSINTERFACE_OBJS) $(MQTT_OBJS) $(KAFKA_OBJS) $(PSQL_OBJS) $(PARSERS_OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: GCC C++ Linker'
	$(CC) $(STD) $(NS_OBJS) $(PROCESSINTERFACE_OBJS) $(MQTT_OBJS) $(KAFKA_OBJS) $(PSQL_OBJS) $(PARSERS_OBJS) -o $(BUILD)/NetworkServer -lmosquittopp -lcryptopp -lrdkafka++ -lpq -lrt -lpthread -lstdc++fs
	@echo 'Finished building target: $@'
	@echo ' '
	
# Other Targets
clean:
	-$(RM)  bin/Release/* bin/Debug/*
	find . -type f -name "*.d" -delete
	find . -type f -name "*.o" -delete
	find . -type f -name "*.a" -delete
	-@echo ' '

.PHONY: all clean dependents config

