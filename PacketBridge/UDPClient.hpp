/*
 * UDPClient.hpp
 *
 *  Created on: Feb 3, 2018
 *      Author: say10
 */

#ifndef UDPCLIENT_HPP_
#define UDPCLIENT_HPP_

#include <string>
#include <vector>
#include <arpa/inet.h>


class UDPClient {
private:
	int client_socket = -1;
	sockaddr_in service;

public:
	UDPClient(void);
	~UDPClient(void);

	bool openChannel(std::string ip, unsigned int port);
	void closeChannel(void);

	void send(std::vector<unsigned char> buffer);
};


#endif /* UDPCLIENT_HPP_ */
