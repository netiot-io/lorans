/*
 * UDPClient.cpp
 *
 *  Created on: Feb 3, 2018
 *      Author: say10
 */

#include "UDPClient.hpp"
#include <sys/socket.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>

UDPClient::UDPClient(void)
{
	memset((char *) &service, 0, sizeof(sockaddr_in));
}

UDPClient::~UDPClient(void)
{
	closeChannel();
}

bool UDPClient::openChannel(std::string ip, unsigned int port)
{
	closeChannel();

	if ((client_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		return false;

	service.sin_family = AF_INET;
	service.sin_port = htons(port);

	inet_aton(ip.c_str(), &service.sin_addr);

	return true;
}

void UDPClient::closeChannel(void)
{
	if (client_socket >= 0) {
		memset((char *) &service, 0, sizeof(sockaddr_in));
		close(client_socket);
	}
}

void UDPClient::send(std::vector<unsigned char> buffer)
{
	sendto(client_socket, buffer.data(), buffer.size(), 0, (sockaddr *) &service, sizeof(sockaddr));
}


