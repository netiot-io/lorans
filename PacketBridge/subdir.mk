
PCKBR_CPP_SRCS += \
PacketBridge/main.cpp \
PacketBridge/UDPClient.cpp \
PacketBridge/UDPServer.cpp \
PacketBridge/PacketManager.cpp

PCKBR_OBJS += \
PacketBridge/main.o \
PacketBridge/UDPClient.o \
PacketBridge/UDPServer.o \
PacketBridge/PacketManager.o

PCKBR_CPP_DEPS += \
PacketBridge/main.d \
PacketBridge/UDPClient.d \
PacketBridge/UDPServer.d \
PacketBridge/PacketManager.d

PCKBR_INCL = \
-IParsers \
-IProcessInterface \
-IMQTTClient


# Each subdirectory must supply rules for building sources it contributes
PacketBridge/%.o: PacketBridge/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) $(PCKBR_INCL) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



