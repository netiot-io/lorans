/*
 * main.cpp
 *
 *  Created on: Feb 3, 2018
 *      Author: say10
 */

#include <string>
#include "PacketManager.hpp"
#include <iostream>

int main(int argc, char **argv)
{
	PacketManager server;

	server.PreRun();
	server.Run();
	server.PostRun();

	return 0;
}
