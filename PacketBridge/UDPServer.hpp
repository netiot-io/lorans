/*
 * UDPServer.hpp
 *
 *  Created on: Feb 3, 2018
 *      Author: say10
 */

#ifndef UDPSERVER_HPP_
#define UDPSERVER_HPP_

#include <vector>
#include <sys/types.h>
#include <arpa/inet.h>

#define DEFAUTL_TIMEOUT {0, 500000}

struct ReceiveData {
	uint32_t recv_addr;
	std::vector<unsigned char> recv_buffer;
};

class UDPServer {
private:
	unsigned int addr_len = sizeof(sockaddr);

	int server_socket = -1;
	unsigned short port = 0;
	fd_set server_fd;

	timeval timeout = DEFAUTL_TIMEOUT;
	std::vector<ReceiveData> gathered_data;
	std::vector<ReceiveData>::iterator findBuffer(uint32_t addr);
	std::vector<uint32_t>::iterator findAddr(uint32_t addr);

	std::vector<uint32_t> ip_addresses;

public:
	UDPServer(void);
	~UDPServer(void);

	bool openChannel(unsigned short port);
	void receive(unsigned int max_bytes);

	bool isChannelOpen(void);

	void closeChannel(void);

	std::vector<unsigned char> getBuffer(uint32_t addr);
	void consumeBuffer(uint32_t addr, unsigned int bytes);

	void setTimeout(unsigned long timeout);

	std::vector<uint32_t> getReceiveAddresses(void);

	void sendBuffer(uint32_t addr, std::vector<unsigned char> buffer);
};


#endif /* UDPSERVER_HPP_ */
