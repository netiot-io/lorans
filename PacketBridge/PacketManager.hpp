/*
 * PacketManager.hpp
 *
 *  Created on: Feb 25, 2018
 *      Author: say10
 */

#ifndef PACKETMANAGER_HPP_
#define PACKETMANAGER_HPP_

#define PKT_PUSH_DATA   0x00
#define PKT_PUSH_ACK    0x01
#define PKT_PULL_DATA   0x02
#define PKT_PULL_RESP   0x03
#define PKT_PULL_ACK    0x04
#define PKT_TX_ACK      0x05

#include <vector>
#include <map>
#include <ProcessInterface.hpp>
#include <MQTTClient.hpp>
#include "UDPServer.hpp"

class PacketManager : ProcessInterface {
private:
	MQTTClient mqtt;

	std::map<uint32_t, bool> canSendPullData;

	UDPServer server;

	size_t ManageData(const uint8_t *data, size_t size, uint32_t addr);
	void ForwardData(const uint8_t *data, size_t size, std::string gatewayMAC);
	void SendToGateways(std::vector<uint8_t> data);
	void VerifyAvailableNSData(void);

	static std::string BinaryToHex(std::vector<uint8_t> bin);

public:
	PacketManager(void);
	~PacketManager(void);

	void PreRun(void) override;
	void Run(void) override;
	void PostRun(void) override;
};



#endif /* PACKETMANAGER_HPP_ */
