/*
 * UDPServer.cpp
 *
 *  Created on: Feb 3, 2018
 *      Author: say10
 */

#include <algorithm>
#include "UDPServer.hpp"
#include <sys/socket.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <iostream>

UDPServer::UDPServer(void)
{
	FD_ZERO(&server_fd);
}

UDPServer::~UDPServer(void)
{
	closeChannel();
}

bool UDPServer::openChannel(unsigned short port)
{
	closeChannel();

	this->port = port;

	sockaddr_in service;

	if ((server_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		return false;

	memset((char *) &service, 0, sizeof(sockaddr_in));
	service.sin_family = AF_INET;
	service.sin_port = htons(port);
	service.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(server_socket, (sockaddr *) &service, sizeof(sockaddr_in)) < 0) {
		close(server_socket);
		server_socket = -1;

		return false;
	}

	long flags = fcntl(server_socket, F_GETFL, 0);
	fcntl(server_socket, F_SETFL, flags | O_NONBLOCK);

	FD_SET(server_socket, &server_fd);

	return true;
}

bool UDPServer::isChannelOpen(void)
{
	return server_socket >= 0;
}

void UDPServer::closeChannel(void)
{
	if (server_socket >= 0) {
		FD_CLR(server_socket, &server_fd);
		close(server_socket);
		server_socket = -1;
	}
}

void UDPServer::setTimeout(unsigned long timeout)
{
	this->timeout.tv_sec = timeout / 1000000;
	this->timeout.tv_usec = timeout % 1000000;
}

void UDPServer::receive(unsigned int max_bytes)
{

	fd_set active_fd;
	timeval tv = timeout;
	int sel_num;
	long recv_bytes;
	char buffer[max_bytes];

	FD_ZERO(&active_fd);

	memcpy(&active_fd, &server_fd, sizeof(fd_set));

	if ((sel_num = select(server_socket + 1, &active_fd, NULL, NULL, &tv)) < 0)
		return;

	if (sel_num == 0)
		return;

	sockaddr_in recv_addr;

	if ((recv_bytes = recvfrom(server_socket, buffer, max_bytes, 0, (sockaddr *) &recv_addr, &addr_len)) < 0)
		return;

	std::vector<uint32_t>::iterator addr_it = findAddr(recv_addr.sin_addr.s_addr);

	if (addr_it == ip_addresses.end()) {
		ip_addresses.push_back(recv_addr.sin_addr.s_addr);

		ReceiveData recv_data;

		recv_data.recv_addr = recv_addr.sin_addr.s_addr;

		gathered_data.push_back(recv_data);
	}

	std::vector<ReceiveData>::iterator data_it = UDPServer::findBuffer(recv_addr.sin_addr.s_addr);

	data_it->recv_buffer.insert(data_it->recv_buffer.end(), buffer, buffer + recv_bytes);
}

std::vector<ReceiveData>::iterator UDPServer::findBuffer(uint32_t addr)
{
	return std::find_if(gathered_data.begin(), gathered_data.end(), [addr] (ReceiveData x)->bool {return x.recv_addr == addr;});
}

std::vector<uint32_t>::iterator UDPServer::findAddr(uint32_t addr)
{
	return std::find_if(ip_addresses.begin(), ip_addresses.end(), [addr] (uint32_t x)->bool {return x == addr;});
}

void UDPServer::consumeBuffer(uint32_t addr, unsigned int bytes)
{
	if (bytes == 0)
		return;

	std::vector<ReceiveData>::iterator data_it = UDPServer::findBuffer(addr);

	if (data_it == gathered_data.end())
		return;

	if (data_it->recv_buffer.begin() + bytes < data_it->recv_buffer.end())
		data_it->recv_buffer = std::vector<unsigned char>(data_it->recv_buffer.begin() + bytes, data_it->recv_buffer.end());
	else
		data_it->recv_buffer.clear();
}

std::vector<unsigned char> UDPServer::getBuffer(uint32_t addr)
{
	std::vector<ReceiveData>::iterator data_it = UDPServer::findBuffer(addr);

	if (data_it == gathered_data.end())
		return std::vector<unsigned char>();

	return data_it->recv_buffer;
}

std::vector<uint32_t> UDPServer::getReceiveAddresses(void)
{
	return ip_addresses;
}

void UDPServer::sendBuffer(uint32_t addr, std::vector<unsigned char> buffer)
{
	sockaddr_in client;

	client.sin_family = AF_INET;
	client.sin_port = htons(port);
	client.sin_addr.s_addr = addr;

	int data_sent = 0;
	size_t data_to_send = buffer.size();
	unsigned char *pos = buffer.data();

	while ((data_sent = sendto(server_socket, pos, data_to_send, 0, (sockaddr *) &client, sizeof(sockaddr))) > 0) {
		pos += data_sent;
		data_to_send -= data_sent;
	}

	if (data_sent >= 0)
		std::cerr << "\nUDP send succeded!\n";
	else
		std::cerr << "\nUDP send failed!\n";
}




