/*
 * PacketManager.cpp
 *
 *  Created on: Feb 25, 2018
 *      Author: Say10
 */

#include <random>
#include <chrono>
#include "PacketManager.hpp"
#include <iostream>
#include <thread>
#include <Json.hpp>
#include <string.h>
#include <Json.hpp>

PacketManager::PacketManager(void)
{
	mosqpp::lib_init();
}

PacketManager::~PacketManager(void)
{
	mosqpp::lib_cleanup();
}

std::string PacketManager::BinaryToHex(std::vector<uint8_t> bin)
{
	char table[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	std::string encoded;
	encoded.resize(bin.size() * 2);

	for (size_t i = 0; i < bin.size(); ++i) {
		encoded[2 * i] = table[(bin[i] >> 4) & 0x0F];
		encoded[2 * i + 1] = table[bin[i] & 0x0F];
	}

	return encoded;
}

size_t PacketManager::ManageData(const uint8_t *data, size_t size, uint32_t addr)
{
	if (size < 4)
		return 0;

	uint8_t token[2];
	std::vector<uint8_t> gatewayMAC(8, 0);

	token[0] = data[1];
	token[1] = data[2];

	std::vector<uint8_t> response;

	if (data[3] == PKT_PUSH_DATA) {
		if (size <= 12)
			return 0;

		size_t lastPos;
		int count = 0;

		for (lastPos = 12; lastPos < size; ++lastPos) {
			if (data[lastPos] == '{')
				++count;
			else if (data[lastPos] == '}')
				--count;

			if (count == 0)
				break;
		}

		if (count != 0)
			return 0;

		for (unsigned int i = 0; i < 8; ++i) {
			gatewayMAC[i] = data[4 + i];
		}

		response.resize(4);

		response[0] = 2;
		response[1] = token[0];
		response[2] = token[1];
		response[3] = PKT_PUSH_ACK;

		server.sendBuffer(addr, response);

		Json finalJSON(std::string(data + 12, data + lastPos + 1));
		long now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

		finalJSON["timestamp"].SetValue(std::to_string(now));

		std::string gwStr = BinaryToHex(gatewayMAC);

		std::cerr << "\nReceived data from gateway " << gwStr << '\n';

		std::string finalJSONstr = Json::ToString(finalJSON);

		ForwardData((uint8_t *) finalJSONstr.c_str(), finalJSONstr.size(), gwStr);

		return lastPos + 1;
	} else if (data[3] == PKT_PULL_DATA) {
		if (size < 12)
			return 0;

		for (unsigned int i = 0; i < 8; ++i)
			gatewayMAC[i] = data[4 + i];

		response.resize(4);

		response[0] = 2;
		response[1] = token[0];
		response[2] = token[1];
		response[3] = PKT_PULL_ACK;

		server.sendBuffer(addr, response);

		canSendPullData[addr] = true;

		return 12;
	} else if (data[3] == PKT_TX_ACK) {
		if (size < 12)
			return 0;

		std::cerr << "Received tx ack: " << std::string(data + 12, data + size) << '\n';

		return size;
	}

	return 4;
}

void PacketManager::ForwardData(const uint8_t *data, size_t size, std::string gatewayMAC)
{
	std::string json(data, data + size);

	std::cerr << "\nSending data: " << json << '\n';

	mqtt.publish(NULL, (mqtt.sendTopic + "/" + gatewayMAC).c_str(), size, data, mqtt.recommandedQoS, false);
}

void PacketManager::PreRun(void)
{
	SetupSignalHandlers();

	mqtt.SetUpMQTT();

	if (mqtt.connect(mqtt.brokerAddress.c_str(), mqtt.mqttPort, 0) != MOSQ_ERR_SUCCESS) {
		std::cerr << "\nCouldn't connect to MQTT Broker!\n";

		return;
	}

	mqtt.subscribe(NULL, mqtt.receiveTopic.c_str(), mqtt.recommandedQoS);

	server.openChannel(1680);

	if (!server.isChannelOpen()) {
		std::cerr << "\nCould not create an UDP server!\n";

		return;
	}

	running = true;
}

void PacketManager::Run(void)
{
	while (running) {
		mqtt.try_loop(1000);

		server.receive(4096);

		for (uint32_t addr : server.getReceiveAddresses()) {
			std::vector<uint8_t> data = server.getBuffer(addr);

			size_t toConsume = ManageData(data.data(), data.size(), addr);

			if (toConsume == 0 && data.size() >= 4096)
				server.consumeBuffer(addr, data.size());
			else
				server.consumeBuffer(addr, toConsume);
		}

		VerifyAvailableNSData();
	}
}

void PacketManager::PostRun(void)
{
	mqtt.disconnect();
	server.closeChannel();

	std::cerr << "\nExiting...\n";
}

void PacketManager::SendToGateways(std::vector<uint8_t> data)
{
	std::vector<uint8_t> prepared(data.size() + 4);

	std::mt19937 generator(std::chrono::high_resolution_clock::now().time_since_epoch().count());

	uint16_t token = (uint16_t) generator();

	prepared[0] = 0x02;
	prepared[1] = token & 0x00FF;
	prepared[2] = (token >> 8) & 0x00FF;
	prepared[4] = PKT_PULL_RESP;

	memcpy(prepared.data() + 4, data.data(), data.size());

	for (std::pair<uint32_t, bool> elem : canSendPullData)
		server.sendBuffer(elem.first, prepared);
}

void PacketManager::VerifyAvailableNSData(void)
{
	size_t count = mqtt.get_message_count();

	for (size_t i = 0; i < count; ++i) {
		submessage message = mqtt.get_message();

		std::cerr << std::string(message.message.begin(), message.message.end()) << '\n';

		SendToGateways(message.message);
	}
}


