/*
 * NSUtils.hpp
 *
 *  Created on: Mar 16, 2018
 *      Author: Say10
 */

#ifndef UTILS_NSUTILS_HPP_
#define UTILS_NSUTILS_HPP_

#include <vector>
#include <string>
#include <variant>
#include <base64.h>

#define MHDR_MTYPE_MASK 0xE0

#define MHDR_JOIN_REQUEST 0x00
#define MHDR_JOIN_ACCEPT 0x20
#define MHDR_UNCONFIRMED_UP 0x40
#define MHDR_UNCONFIRMED_DOWN 0x60
#define MHDR_CONFIRMED_UP 0x80
#define MHDR_CONFIRMED_DOWN 0xA0
#define MHDR_RFU 0xC0
#define MHDR_PROPRIETARY 0xE0

#define NETWORK_ID 0xFE000000

namespace NSUtils {
	enum PayloadType {
		None,
		JoinRequestType,
		JoinAcceptType,
		MACPayloadType
	};

	class MACPayload {
	public:
		bool IsConfirmed = false;

		uint8_t DevAddr[4] = {0x00, 0x00, 0x00, 0x00};
		uint8_t FCtrl = 0x00;
		uint8_t FCnt[4] = {0x00, 0x00, 0x00, 0x00};
		std::vector<uint8_t> FOpt;
		uint8_t FPort = 0x00;
		std::vector<uint8_t> FRM;

		MACPayload(void);
		~MACPayload(void);

		MACPayload(MACPayload &obj);
		MACPayload &operator=(MACPayload &obj);
	};

	class JoinRequest {
	public:
		uint8_t AppEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		uint8_t DevEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		uint8_t DevNonce[2] = {0x00, 0x00};

		JoinRequest(void);
		~JoinRequest(void);

		JoinRequest(JoinRequest &obj);
		JoinRequest &operator=(JoinRequest &obj);
	};

	class JoinAccept {
	public:
		uint8_t AppNonce[3] = {0x00, 0x00, 0x00};
		uint8_t NetID[3] = {0x00, 0x00, 0x00};
		uint8_t DevAddr[4] = {0x00, 0x00, 0x00, 0x00};
		uint8_t DLSettings = 0x00;
		uint8_t RXDelay = 0x00;
		std::vector<uint8_t> CFList;

		JoinAccept(void);
		~JoinAccept(void);

		JoinAccept(JoinAccept &obj);
		JoinAccept &operator=(JoinAccept &obj);
	};

	class PHYPayload {
	public:
		PHYPayload(void);
		~PHYPayload(void);

		PHYPayload(PayloadType pt);
		PHYPayload(std::vector<uint8_t> payload);

		PHYPayload(PHYPayload &obj);
		PHYPayload &operator=(PHYPayload &obj);

		uint8_t MHDR = 0x00;
		PayloadType pt = None;
		std::variant<MACPayload, JoinAccept, JoinRequest> data;
		uint8_t MIC[4] = {0x00, 0x00, 0x00, 0x00};

		std::vector<uint8_t> Assemble(void);
		void CalculateMIC(std::vector<uint8_t> key);
	};

	std::vector<uint8_t> DecodeBase64(std::string encoded, int maxOutLen);
	std::string BinaryToHex(std::vector<uint8_t> bin);
	std::vector<uint8_t> HexToBinary(std::string hex);
	std::vector<uint8_t> EncryptCBC(std::vector<uint8_t> buffer, std::vector<uint8_t> key);
	std::vector<uint8_t> EncryptEBC(std::vector<uint8_t> buffer, std::vector<uint8_t> key);
	std::vector<uint8_t> DecryptEBC(std::vector<uint8_t> buffer, std::vector<uint8_t> key);
	std::vector<uint8_t> CMAC(std::vector<uint8_t> buffer, std::vector<uint8_t> key);
	std::vector<uint8_t> DecryptMACPayload(PHYPayload payload, std::vector<uint8_t> key);
	std::vector<uint8_t> CreateMIC(PHYPayload payload, std::vector<uint8_t> key);
	bool VerifyMIC(PHYPayload payload, std::vector<uint8_t> key);

	uint16_t GenerateRandom16(void);
	uint32_t GenerateRandom32(void);

	std::string AddTime(std::string dateTime, unsigned long seconds);

	std::vector<std::string> TokenizeString(std::string str, std::string delim, bool keepEmpty = true);
}

#endif /* UTILS_NSUTILS_HPP_ */
