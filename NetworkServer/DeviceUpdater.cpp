/*
 * DeviceUpdater.cpp
 *
 *  Created on: Feb 9, 2019
 *      Author: networkserver
 */

#include "DeviceUpdater.hpp"
#include <Query.hpp>
#include <iostream>


void DeviceUpdater::InsertDevice(PGconn *pgconnection, Json deviceInfo)
{
	std::string qstr;
	Query inserter("ns_schema.devices");

	inserter.AddColumnValue("id", deviceInfo["id"].GetNumber());

	Json params = deviceInfo["parameters"].GetJson();
	inserter.AddColumnValue("deveui", params["devEui"].GetString());

	if (params["activationMethod"].GetString() == "ABP") {
		inserter.AddColumnValue("activation_method", "ABP");
		inserter.AddColumnValue("devaddr", params["devAddr"].GetString());
		inserter.AddColumnValue("nwskey", params["nwksKey"].GetString());
		inserter.AddColumnValue("appskey", params["appsKey"].GetString());
	} else {
		inserter.AddColumnValue("activation_method", "OTAA");
		inserter.AddColumnValue("appeui", params["appEui"].GetString());
		inserter.AddColumnValue("appkey", params["appKey"].GetString());
	}

	inserter.AddColumnValue("last_contact", (double) 0);
	inserter.AddColumnValue("fcntup", (double) 0);
	inserter.AddColumnValue("fcntdown", (double) 0);
	inserter.AddColumnValue("active", deviceInfo["statusEventType"].GetString() == "ACTIVE");
	qstr = inserter.GetInsertString();

	PGresult *result = NULL;

	result = PQexec(pgconnection, qstr.c_str());

	std::cerr << qstr << "\n";

	if (result == NULL || PQresultStatus(result) != PGRES_COMMAND_OK)
		std::cerr << "\nCouldn't execute SQL command!\n";

	if (result != NULL)
		PQclear(result);
}

void DeviceUpdater::ProcessUpdate(PGconn *pgconnection, Json deviceInfo)
{
	if (deviceInfo["eventType"].GetString() == "CREATED")
		InsertDevice(pgconnection, deviceInfo);
	else if (deviceInfo["eventType"].GetString() == "DELETED")
		DeleteDevice(pgconnection, deviceInfo);
	else if (deviceInfo["eventType"].GetString() == "CHANGED")
		ChangeDevice(pgconnection, deviceInfo);
	else
		std::cerr << "Unknown event type :" << deviceInfo["eventType"].GetString() << '\n';
}

void DeviceUpdater::ChangeDevice(PGconn *pgconnection, Json deviceInfo)
{
	Query updater("ns_schema.devices");

	Json params = deviceInfo["parameters"].GetJson();

	if (params["activationMethod"].GetString() == "ABP") {
		updater.AddColumnValue("activation_method", "ABP");
		updater.AddColumnValue("devaddr", params["devAddr"].GetString());
		updater.AddColumnValue("nwskey", params["nwksKey"].GetString());
		updater.AddColumnValue("appskey", params["appsKey"].GetString());
	} else {
		updater.AddColumnValue("activation_method", "OTAA");
		updater.AddColumnValue("appeui", params["appEui"].GetString());
		updater.AddColumnValue("appkey", params["appKey"].GetString());
	}

	updater.AddColumnValue("active", deviceInfo["statusEventType"].GetString() == "ACTIVE");
	updater.SetCondition("id = '" + std::to_string((long) deviceInfo["id"].GetNumber()) + '\'');

	std::string qstr = updater.GetUpdateString();
	std::cerr << qstr << "\n";

	PGresult *result = NULL;
	result = PQexec(pgconnection, qstr.c_str());

	if (result == NULL || PQresultStatus(result) != PGRES_COMMAND_OK)
		std::cerr << "\nCouldn't execute SQL command!\n";

	if (result != NULL)
		PQclear(result);
}

void DeviceUpdater::DeleteDevice(PGconn *pgconnection, Json deviceInfo)
{
	Query deleter("ns_schema.devices");
	deleter.SetCondition("id = " + std::to_string((long) deviceInfo["id"].GetNumber()));
	
	std::string qstr = deleter.GetDeleteString();
	std::cerr << qstr << "\n";

	PGresult *result = NULL;
	result = PQexec(pgconnection, qstr.c_str());

	if (result == NULL || PQresultStatus(result) != PGRES_COMMAND_OK)
		std::cerr << "\nCouldn't execute SQL command!\n";

	if (result != NULL)
		PQclear(result);
}
