
NS_CPP_SRCS += \
NetworkServer/main.cpp \
NetworkServer/DeviceUpdater.cpp \
NetworkServer/NSUtils.cpp \
NetworkServer/NetworkServer.cpp

NS_OBJS += \
NetworkServer/main.o \
NetworkServer/DeviceUpdater.o \
NetworkServer/NSUtils.o \
NetworkServer/NetworkServer.o

NS_CPP_DEPS += \
NetworkServer/main.d \
NetworkServer/DeviceUpdater.d \
NetworkServer/NSUtils.d \
NetworkServer/NetworkServer.d

NS_INCL = \
-IProcessInterface \
-IParsers \
-IMQTTClient \
-IKafkaClient \
-IPostgreSQL


# Each subdirectory must supply rules for building sources it contributes
NetworkServer/%.o: NetworkServer/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) $(NS_INCL) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



