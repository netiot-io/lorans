/*
 * main.cpp
 *
 *  Created on: Mar 13, 2018
 *      Author: Say10
 */

#include "NetworkServer.hpp"
#include <iostream>

int main(int argc, char **argv)
{
	NetworkServer ns;

	ns.PreRun();
	ns.Run();
	ns.PostRun();

	return 0;
}


