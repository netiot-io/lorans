/*
 * NetworkServer.hpp
 *
 *  Created on: Mar 11, 2018
 *      Author: Say10
 */

#ifndef NETWORKSERVER_HPP_
#define NETWORKSERVER_HPP_

#include <chrono>
#include <fstream>
#include <ProcessInterface.hpp>
#include <MQTTClient.hpp>
#include "NSUtils.hpp"
#include <Json.hpp>
#include <optional>
#include <KafkaClient.hpp>
#include <PostgresConnection.hpp>

class NetworkServer : public ProcessInterface {
private:
	MQTTClient mqtt;
	PostgresConnection dbConnector;

	KafkaClient kafkaClient;
	std::list<Json> dataAccumulator;

	std::chrono::milliseconds lastDelete = std::chrono::milliseconds(0);

	void InspectData(std::string &data, std::string gwMAC);
	void StoreDeviceData(Json parsed, long timestamp);
	void SavePacket(Json packet, std::string deveui, long timestamp);
	void SendDataToGateways(std::vector<uint8_t> data, std::string dateTime = "");

	std::string AnalyseMACDataAndGetDevEUI(NSUtils::PHYPayload macData, long timestamp, Json objectToSend);
	std::string JoinProcedureAndGetDevEUI(NSUtils::PHYPayload request, long timestamp, std::string dateTime);

	void CheckDeviceUpdates(void);

	void UpdateDeviceFCntUp(std::string deveui, int fcntup, long timestamp);
	void UpdateDeviceFCntDown(std::string deveui, int fcntdown);
	void UpdateDeviceFCntUpAndDown(std::string deveui, int fcntup, int fcntdown, long timestamp);
	void DeletePacketsPastDay(void);

	void ForwardData(void);

	static std::vector<uint8_t> CreateAckResponse(std::vector<uint8_t> devAddr, int fcntdown, uint8_t fport, std::vector<uint8_t> key);
public:
	NetworkServer(void);
	~NetworkServer(void);

	void PreRun(void) override;
	void Run(void) override;
	void PostRun(void) override;
};

#endif /* NETWORKSERVER_HPP_ */
