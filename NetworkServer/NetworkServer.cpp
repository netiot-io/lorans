/*
 * NetworkServer.cpp
 *
 *  Created on: Mar 11, 2018
 *      Author: Say10
 */

#include "NetworkServer.hpp"
#include <iostream>
#include <cmath>
#include <cstring>
#include <sstream>
#include <signal.h>
#include <algorithm>
#include <sys/wait.h>
#include "DeviceUpdater.hpp"
#include <Query.hpp>
#include "DeviceUpdater.hpp"

NetworkServer::NetworkServer(void)
{
	mosqpp::lib_init();
}

NetworkServer::~NetworkServer(void)
{
	mosqpp::lib_cleanup();
}

void NetworkServer::SavePacket(Json packet, std::string deveui, long timestamp)
{
	PGresult *result = NULL;
	Query query("ns_schema.rfpackets");

	query.AddColumnValue("deveui", deveui);
	query.AddColumnValue("received_time", (double) timestamp);

	if (packet.HasField("time"))
		query.AddColumnValue("time", '\'' + packet["time"].GetString() + '\'');

	query.AddColumnValue("tmms", packet["tmms"].GetNumber());
	query.AddColumnValue("tmst", packet["tmst"].GetNumber());
	query.AddColumnValue("freq", packet["freq"].GetNumber());
	query.AddColumnValue("chan", packet["chan"].GetNumber());
	query.AddColumnValue("rfch", packet["rfch"].GetNumber());
	query.AddColumnValue("stat", packet["stat"].GetNumber());

	std::string modu = packet["modu"].GetString();

	query.AddColumnValue("modu", modu);

	if (modu == "LORA") {
		query.AddColumnValue("datr", packet["datr"].GetString());
		query.AddColumnValue("codr", packet["codr"].GetString());
		query.AddColumnValue("lsnr", packet["lsnr"].GetNumber());
	} else
		query.AddColumnValue("datr", std::to_string(packet["datr"].GetNumber()));

	query.AddColumnValue("size", packet["size"].GetNumber());
	query.AddColumnValue("data", packet["data"].GetString());

	std::cerr << query.GetInsertString().c_str() << '\n';

	result = PQexec(dbConnector.pgconnection, query.GetInsertString().c_str());

	if (result != NULL && PQresultStatus(result) == PGRES_COMMAND_OK)
		std::cerr << "\nSaved radio packet!\n";

	if (result != NULL)
		PQclear(result);
}

void NetworkServer::StoreDeviceData(Json parsed, long timestamp)
{
	std::vector<Json::Element> packets = parsed["rxpk"].GetArray();
	std::vector<NSUtils::PHYPayload> disassembledPayloads;
	Json objectToSend;
	std::string devEUI;

	objectToSend["timestamp"].SetValue(std::to_string(timestamp));

	for (Json::Element &packet : packets) {
		Json obj = packet.GetJson();

		std::vector<uint8_t> rawdata = NSUtils::DecodeBase64(obj["data"].GetString(), obj["size"].GetNumber());

		NSUtils::PHYPayload disassembled(rawdata);

		if (disassembled.pt == NSUtils::MACPayloadType)
			devEUI = AnalyseMACDataAndGetDevEUI(disassembled, timestamp, objectToSend);
		else if (disassembled.pt == NSUtils::JoinRequestType)
			devEUI = JoinProcedureAndGetDevEUI(disassembled, timestamp, obj["time"].GetString());
		else
			devEUI = "";

		if (devEUI != "")
			SavePacket(obj, devEUI, timestamp);
	}
}

void NetworkServer::InspectData(std::string &data, std::string gwMAC)
{
	std::cerr << "\nInspecting message: \n" << data << " from " << gwMAC << '\n';

	Json json(data);

	StoreDeviceData(json, strtol(json["timestamp"].GetString().c_str(), NULL, 10));
}

void NetworkServer::PreRun(void)
{
	SetupSignalHandlers();

	kafkaClient.SetUpKafkaClient();

	if (!kafkaClient.IsConfigured())
		return;

	mqtt.SetUpMQTT();

	mqtt.try_connection();

	mqtt.subscribe(NULL, (mqtt.receiveTopic + "/#").c_str(), mqtt.recommandedQoS);

	dbConnector.InitDBConnection();

	std::vector<std::string> topics;
	topics.push_back(kafkaClient.ConsumeTopic());

	kafkaClient.SetSubscriptions(topics);

	running = true;
}

void NetworkServer::ForwardData(void)
{
	if (!dataAccumulator.empty()) {
		Json devices;
		std::vector<Json::Element> objects(dataAccumulator.size());
		std::transform(dataAccumulator.begin(), dataAccumulator.end(), objects.begin(),
					[] (Json json) -> Json::Element { Json::Element e; e.SetValue(json); return e; } );
		devices["devices"].SetValue(objects);
		kafkaClient.Produce(kafkaClient.ProduceTopic(), Json::ToString(devices));

		dataAccumulator.clear();
	}

	kafkaClient.Pool(1000);
}

void NetworkServer::Run(void)
{
	while (running) {
		if (dbConnector.pgconnection == NULL)
			break;

		mqtt.try_loop(5000);

		size_t messageCount = mqtt.get_message_count();

		for (size_t i = 0; i < messageCount; ++i) {
			submessage message = mqtt.get_message();

			std::vector<std::string> subtopics = NSUtils::TokenizeString(std::string(message.topic), "/");
			std::string gwMAC = subtopics[subtopics.size() - 1];
			std::string jsonStr(message.message.begin(), message.message.end());

			InspectData(jsonStr, gwMAC);
		}

		ForwardData();
		DeletePacketsPastDay();

		CheckDeviceUpdates();
	}
}

void NetworkServer::CheckDeviceUpdates(void)
{
	std::map<std::string, std::list<std::string>> messages = kafkaClient.GetMessages();

	for (std::pair<const std::string, std::list<std::string>> messageList : messages) {
		std::cerr << "Messages from topic: " << messageList.first << "\n";

		for (std::string message : messageList.second) {
			std::cerr << "\t" << message << "\n";
			DeviceUpdater::ProcessUpdate(dbConnector.pgconnection, Json(message));
		}	
	}
}

void NetworkServer::PostRun(void)
{
	mqtt.disconnect();
	dbConnector.EndDBConnection();
}

std::string NetworkServer::JoinProcedureAndGetDevEUI(NSUtils::PHYPayload request, long timestamp, std::string dateTime)
{
	PGresult *result = NULL;
	Query query("ns_schema.devices");

	NSUtils::JoinRequest joinRequest = std::get<NSUtils::JoinRequest>(request.data);

	std::string devEUI = NSUtils::BinaryToHex(std::vector<uint8_t>(joinRequest.DevEUI, joinRequest.DevEUI + 8));

	query.AddSelectedColumn("appkey");
	query.AddSelectedColumn("appeui");

	query.SetCondition("deveui = '" + devEUI + "' and activation_method = 'OTAA'");

	result = PQexec(dbConnector.pgconnection, query.GetSelectString().c_str());

	if (result == NULL) {
		std::cerr << "\nDevice not found!\n";

		return "";
	}

	if (PQresultStatus(result) != PGRES_TUPLES_OK || PQntuples(result) != 1) {
		std::cerr << "\nDevice not found!\n";

		PQclear(result);

		return "";
	}

	std::vector<uint8_t> appKey = NSUtils::HexToBinary(PQgetvalue(result, 0, 0));
	std::vector<uint8_t> appEUI = NSUtils::HexToBinary(PQgetvalue(result, 0, 1));

	PQclear(result);

	for (int i = 0; i < 8; ++i)
		if (joinRequest.AppEUI[i] != appEUI[i]) {
			std::cerr << "\nAppEUI doesn't match!\n";

			return "";
		}

	uint32_t appNonce = NSUtils::GenerateRandom32();

	NSUtils::PHYPayload accept(NSUtils::JoinAcceptType);

	accept.MHDR = MHDR_JOIN_ACCEPT;

	NSUtils::JoinAccept joinAccept = std::get<NSUtils::JoinAccept>(accept.data);

	joinAccept.AppNonce[0] = appNonce & 0xFF;
	joinAccept.AppNonce[1] = (appNonce >> 8) & 0xFF;
	joinAccept.AppNonce[2] = (appNonce >> 16) & 0xFF;

	uint32_t devAddr = (NSUtils::GenerateRandom32() & 0x01FFFFFF) | NETWORK_ID;

	joinAccept.DevAddr[0] = (devAddr >> 24) & 0xFF;
	joinAccept.DevAddr[1] = (devAddr >> 16) & 0xFF;
	joinAccept.DevAddr[2] = (devAddr >> 8) & 0xFF;
	joinAccept.DevAddr[3] = devAddr & 0xFF;

	uint32_t netID = (NSUtils::GenerateRandom32() & 0xFFFFFF80) | (NETWORK_ID >> 25);

	joinAccept.NetID[0] = (netID >> 16) & 0xFF;
	joinAccept.NetID[1] = (netID >> 8) & 0xFF;
	joinAccept.NetID[2] = netID & 0xFF;

	accept.data = joinAccept;

	accept.CalculateMIC(appKey);

	std::vector<uint8_t> bufferToSend = accept.Assemble();

	std::cerr << "\nTo encrypt: " << NSUtils::BinaryToHex(std::vector<uint8_t>(bufferToSend.begin() + 1, bufferToSend.end())) << '\n';

	std::vector<uint8_t> encryptedPart = NSUtils::DecryptEBC(std::vector<uint8_t>(bufferToSend.begin() + 1, bufferToSend.end()), appKey);

	std::vector<uint8_t> generatingBlock(16, 0);

	generatingBlock[0] = 0x01;
	generatingBlock[1] = joinAccept.AppNonce[0];
	generatingBlock[2] = joinAccept.AppNonce[1];
	generatingBlock[3] = joinAccept.AppNonce[2];
	generatingBlock[4] = joinAccept.NetID[0];
	generatingBlock[5] = joinAccept.NetID[1];
	generatingBlock[6] = joinAccept.NetID[2];
	generatingBlock[7] = joinRequest.DevNonce[0];
	generatingBlock[8] = joinRequest.DevNonce[1];

	std::vector<uint8_t> nwSKey = NSUtils::EncryptEBC(generatingBlock, appKey);

	generatingBlock[0] = 0x02;

	std::vector<uint8_t> appSKey = NSUtils::EncryptEBC(generatingBlock, appKey);

	memcpy(bufferToSend.data() + 1, encryptedPart.data(), encryptedPart.size() - 1);

	query = Query("ns_schema.devices");

	query.AddColumnValue("nwskey", NSUtils::BinaryToHex(nwSKey));
	query.AddColumnValue("appskey", NSUtils::BinaryToHex(appSKey));
	query.AddColumnValue("devaddr", NSUtils::BinaryToHex(std::vector<uint8_t>(joinAccept.DevAddr, joinAccept.DevAddr + 4)));
	query.AddColumnValue("fcntup", (double) 0);
	query.AddColumnValue("fcntdown", (double) 0);
	query.AddColumnValue("last_contact", (double) timestamp);

	query.SetCondition("deveui = '" + devEUI + '\'');

	//std::cerr << query.GetUpdateString() << '\n';

	result = PQexec(dbConnector.pgconnection, query.GetUpdateString().c_str());

	if (result == NULL) {
		std::cerr << "\nDevice not found!\n";

		return "";
	}

	if (PQresultStatus(result) != PGRES_COMMAND_OK) {
		std::cerr << "\nDevice not found!\n";

		PQclear(result);

		return "";
	}

	PQclear(result);

	SendDataToGateways(bufferToSend, NSUtils::AddTime(dateTime, 5));

	return devEUI;
}

void NetworkServer::SendDataToGateways(std::vector<uint8_t> data, std::string dateTime)
{
	std::string converted(2 * data.size(), 0);

	int size = bin_to_b64(data.data(), data.size(), converted.data(), converted.size());

	converted.resize(size);

	std::stringstream toSend;
	toSend << "{\"txpk\":{";

	if (dateTime != "")
		toSend << "\"tmst\":\"" << dateTime << "\"";
	else
		toSend << "\"imme\":true";

	toSend << ",\"freq\":868.1,\"rfch\":0,\"powe\":14,"
			"\"modu\":\"LORA\",\"datr\":\"SF12BW125\",\"codr\":\"4/5\",\"ipol\":false,"
			"\"size\":"<< data.size() <<",\"data\":\"" << converted << "\"}}";

	std::string packet = toSend.str();

	mqtt.publish(NULL, mqtt.sendTopic.c_str(), packet.size(), packet.c_str(), mqtt.recommandedQoS, false);
}

std::string NetworkServer::AnalyseMACDataAndGetDevEUI(NSUtils::PHYPayload macData, long timestamp, Json objectToSend)
{
	PGresult *result = NULL;
	Query query("ns_schema.devices");

	NSUtils::MACPayload data = std::get<NSUtils::MACPayload>(macData.data);

	query.AddSelectedColumn("id");
	query.AddSelectedColumn("active");
	query.AddSelectedColumn("deveui");
	query.AddSelectedColumn("fcntup");
	query.AddSelectedColumn("nwskey");

	int fcntdownPos = 0;
	int appskeyPos = 0;
	uint64_t deviceID = 0;

	if (data.IsConfirmed) {
		fcntdownPos = 5;
		query.AddSelectedColumn("fcntdown");
	}

	if (data.FPort != 0) {
		appskeyPos = fcntdownPos != 0 ? 6 : 5;

		query.AddSelectedColumn("appskey");
	}

	std::vector<uint8_t> devAddr(data.DevAddr, data.DevAddr + 4);
	std::vector<uint8_t> revDevAddr(4);
	std::reverse_copy(devAddr.begin(), devAddr.end(), revDevAddr.begin());

	std::string devaddrStr = NSUtils::BinaryToHex(revDevAddr);

	query.SetCondition("devaddr = '" + devaddrStr + '\'');

	result = PQexec(dbConnector.pgconnection, query.GetSelectString().c_str());

	if (result != NULL && PQresultStatus(result) == PGRES_TUPLES_OK && PQntuples(result) == 1) {
		std::cerr << "\nDevice with DevAddr " << devaddrStr << " found!\n";

		deviceID = strtoul(PQgetvalue(result, 0, 0), NULL, 10);
		bool isActive = PQgetvalue(result, 0, 1)[0] != 'f';
		std::string deveui(PQgetvalue(result, 0, 2));

		int serverFcntup = strtol(PQgetvalue(result, 0, 3), NULL, 10);
		int deviceFcntup = 0;

		std::vector<uint8_t> nwskey = NSUtils::HexToBinary(std::string(PQgetvalue(result, 0, 4)));

		deviceFcntup = ((int) data.FCnt[0]) | (((int) data.FCnt[1]) << 8);

		std::cerr << "\nDevice Fcntup " << deviceFcntup << '\n';
		std::cerr << "Server Fcntup " << serverFcntup << '\n';

		/*if (deviceFcntup > serverFcntup)*/ {
			if (NSUtils::VerifyMIC(macData, nwskey)) {
				std::vector<uint8_t> decrypted;

				if (data.FPort == 0)
					decrypted = NSUtils::DecryptMACPayload(macData, nwskey);
				else {
					std::vector<uint8_t> appskey = NSUtils::HexToBinary(std::string(PQgetvalue(result, 0, appskeyPos)));

					decrypted = NSUtils::DecryptMACPayload(macData, appskey);
				}

				std::cerr << "\nReceived application data: " << NSUtils::BinaryToHex(decrypted) << '\n';

				objectToSend["deviceId"].SetValue((double) deviceID);
				objectToSend["data"].SetValue(NSUtils::BinaryToHex(decrypted));

				dataAccumulator.push_back(objectToSend);

				if (data.IsConfirmed) {
					int serverFcntdown = strtol(PQgetvalue(result, 0, fcntdownPos), NULL, 10);

					std::vector<uint8_t> response = CreateAckResponse(devAddr, serverFcntdown, data.FPort, nwskey);

					UpdateDeviceFCntUpAndDown(deveui, deviceFcntup, ++serverFcntdown, timestamp);

					SendDataToGateways(response);
				} else
					UpdateDeviceFCntUp(deveui, deviceFcntup, timestamp);

				std::cerr << "Device " << deveui << " is " << (isActive ? "active" : "inactive") << '\n';

				PQclear(result);

				return isActive ? deveui : "";
			} else {
				std::cerr << "\nPayload failed MIC verification!\n";
			}
		} //else {
//					std::cerr << "Detected duplicated packet!\n";
//				}

	} else
		std::cerr << "\nDevice with DevAddr " << devaddrStr << " not found!\n";

	if (result != NULL)
		PQclear(result);

	return "";
}

void NetworkServer::UpdateDeviceFCntUp(std::string deveui, int fcntup, long timestamp)
{
	PGresult *result = NULL;
	Query query("ns_schema.devices");

	query.SetCondition("deveui = '" + deveui + '\'');

	query.AddColumnValue("last_contact", (double) timestamp);
	query.AddColumnValue("fcntup", (double) fcntup);

	result = PQexec(dbConnector.pgconnection, query.GetUpdateString().c_str());

	if (result != NULL && PQresultStatus(result) == PGRES_COMMAND_OK)
		std::cerr << "\nUpdated device!\n";
	else
		std::cerr << "\nFailed to update device!\n";

	if (result != NULL)
		PQclear(result);
}

void NetworkServer::UpdateDeviceFCntDown(std::string deveui, int fcntdown)
{
	PGresult *result = NULL;
	Query query("ns_schema.devices");

	query.SetCondition("deveui = '" + deveui + '\'');

	query.AddColumnValue("fcntdown", (double) fcntdown);

	result = PQexec(dbConnector.pgconnection, query.GetUpdateString().c_str());

	if (result != NULL && PQresultStatus(result) == PGRES_COMMAND_OK)
		std::cerr << "\nUpdated device!\n";
	else
		std::cerr << "\nFailed to update device!\n";

	if (result != NULL)
		PQclear(result);
}

void NetworkServer::UpdateDeviceFCntUpAndDown(std::string deveui, int fcntup, int fcntdown, long timestamp)
{
	PGresult *result = NULL;
	Query query("ns_schema.devices");

	query.SetCondition("deveui = '" + deveui + '\'');

	query.AddColumnValue("fcnup", (double) fcntup);
	query.AddColumnValue("fcntdown", (double) fcntdown);
	query.AddColumnValue("timestamp", (double) timestamp);

	result = PQexec(dbConnector.pgconnection, query.GetUpdateString().c_str());

	if (result != NULL && PQresultStatus(result) == PGRES_COMMAND_OK)
		std::cerr << "\nUpdated device!\n";
	else
		std::cerr << "\nFailed to update device!\n";

	if (result != NULL)
		PQclear(result);
}

std::vector<uint8_t> NetworkServer::CreateAckResponse(std::vector<uint8_t> devAddr, int fcntdown, uint8_t fport, std::vector<uint8_t> key)
{
	NSUtils::PHYPayload emptyAck(NSUtils::MACPayloadType);

	emptyAck.MHDR = MHDR_UNCONFIRMED_DOWN;

	NSUtils::MACPayload data = std::get<NSUtils::MACPayload>(emptyAck.data);

	memcpy(data.DevAddr, devAddr.data(), 4);

	data.FCnt[0] = fcntdown & 0XFF;
	data.FCnt[1] = (fcntdown >> 8) & 0XFF;

	data.FPort = fport;

	emptyAck.CalculateMIC(key);

	return emptyAck.Assemble();
}

void NetworkServer::DeletePacketsPastDay(void)
{
	std::chrono::milliseconds now = std::chrono::duration_cast<std::chrono::milliseconds>
									(std::chrono::high_resolution_clock::now().time_since_epoch());

	if (now - lastDelete <= std::chrono::hours(1))
		return;

	lastDelete = now;

	std::chrono::milliseconds then = now - std::chrono::hours(24);

	Query deleter("ns_schema.rfpackets"), subquerry("ns_schema.rfpackets");

	subquerry.AddSelectedColumn("id");
	subquerry.SetCondition("received_time < " + std::to_string(then.count()));

	std::stringstream condition;
	condition << "id in (" << subquerry.GetSelectString() << ")";

	deleter.SetCondition(condition.str());

	PGresult *result = NULL;

	result = PQexec(dbConnector.pgconnection, deleter.GetDeleteString().c_str());

	if (result != NULL && PQresultStatus(result) == PGRES_COMMAND_OK)
		std::cerr << "\nDeleted old packets!\n";
	else
		std::cerr << "\nFailed to delete old packets!\n";

	if (result != NULL)
		PQclear(result);
}
