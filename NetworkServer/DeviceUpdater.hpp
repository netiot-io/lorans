/*
 * DeviceUpdater.hpp
 *
 *  Created on: Feb 9, 2019
 *      Author: networkserver
 */

#include <PostgresConnection.hpp>
#include <Json.hpp>

namespace DeviceUpdater {
	void InsertDevice(PGconn *pgconnection, Json deviceInfo);
	void ProcessUpdate(PGconn *pgconnection, Json deviceInfo);
	void ChangeDevice(PGconn *pgconnection, Json deviceInfo);
	void DeleteDevice(PGconn *pgconnection, Json deviceInfo);
};
