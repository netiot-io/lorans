/*
 * NSUtils.cpp
 *
 *  Created on: Mar 16, 2018
 *      Author: Say10
 */

#include <algorithm>
#include <functional>
#include <cmath>
#include <cstring>
#include <cryptopp/aes.h>
#include <cryptopp/cmac.h>
#include <cryptopp/modes.h>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <signal.h>
#include <iostream>
#include <chrono>
#include <random>
#include <ctime>
#include "NSUtils.hpp"

#define FCTRL_FOPT_SIZE_MASK 0x07

NSUtils::JoinAccept::JoinAccept(void) {}
NSUtils::JoinAccept::~JoinAccept(void) {}

NSUtils::JoinAccept::JoinAccept(JoinAccept &obj)
{
	*this = obj;
}

NSUtils::JoinAccept &NSUtils::JoinAccept::operator=(JoinAccept &obj)
{
	memcpy(AppNonce, obj.AppNonce, 3);
	memcpy(NetID, obj.NetID, 3);
	memcpy(DevAddr, obj.DevAddr, 4);
	DLSettings = obj.DLSettings;
	RXDelay = obj.RXDelay;
	CFList = obj.CFList;

	return *this;
}

NSUtils::JoinRequest::JoinRequest(void) {}
NSUtils::JoinRequest::~JoinRequest(void) {}

NSUtils::JoinRequest::JoinRequest(JoinRequest &obj)
{
	*this = obj;
}

NSUtils::JoinRequest &NSUtils::JoinRequest::operator=(JoinRequest &obj)
{
	memcpy(AppEUI, obj.AppEUI, 8);
	memcpy(DevEUI, obj.DevEUI, 8);
	DevNonce[0] = obj.DevNonce[0];
	DevNonce[1] = obj.DevNonce[1];

	return *this;
}

NSUtils::MACPayload::MACPayload(void) {}
NSUtils::MACPayload::~MACPayload(void) {}

NSUtils::MACPayload::MACPayload(MACPayload &obj)
{
	*this = obj;
}

NSUtils::MACPayload &NSUtils::MACPayload::operator=(MACPayload &obj)
{
	memcpy(DevAddr, obj.DevAddr, 4);
	memcpy(FCnt, obj.FCnt, 4);
	FPort = obj.FPort;
	FCtrl = obj.FCtrl;
	FOpt = obj.FOpt;
	FRM = obj.FRM;
	IsConfirmed = obj.IsConfirmed;

	return *this;
}

NSUtils::PHYPayload::PHYPayload(void) {}

NSUtils::PHYPayload::~PHYPayload(void) {}

NSUtils::PHYPayload::PHYPayload(PayloadType pt)
{
	this->pt = pt;
	JoinRequest jr;
	JoinAccept ja;
	MACPayload mp;

	switch (pt) {
	case JoinRequestType:
		data = jr;
		break;
	case JoinAcceptType:
		data = ja;
		break;
	case MACPayloadType:
		data = mp;
		break;
	default:
		break;
	}
}

NSUtils::PHYPayload::PHYPayload(std::vector<uint8_t> payload)
{
	size_t payloadSize = payload.size();

	MHDR = payload[0];

	uint8_t payloadType = MHDR & MHDR_MTYPE_MASK;

	if (payloadType == MHDR_UNCONFIRMED_UP || payloadType == MHDR_CONFIRMED_UP) {
		if (payloadSize < 12)
			return;

		pt = MACPayloadType;

		MACPayload macData;

		macData.DevAddr[0] = payload[1];
		macData.DevAddr[1] = payload[2];
		macData.DevAddr[2] = payload[3];
		macData.DevAddr[3] = payload[4];
		macData.FCtrl = payload[5];
		macData.FCnt[0] = payload[6];
		macData.FCnt[1] = payload[7];

		size_t optSize = macData.FCtrl & FCTRL_FOPT_SIZE_MASK;

		macData.FOpt.resize(optSize);
		memcpy(macData.FOpt.data(), payload.data() + 8, optSize);

		if (payloadSize > 13 + optSize) {
			macData.FPort = payload[8 + optSize];
			macData.FRM.resize(payloadSize - 13 - optSize);
			memcpy(macData.FRM.data(), payload.data() + 9 + optSize, macData.FRM.size());
		} else {
			macData.FRM.resize(payloadSize - 12 - optSize);
			memcpy(macData.FRM.data(), payload.data() + 8 + optSize, macData.FRM.size());
		}

		if (payloadType == MHDR_CONFIRMED_UP)
			macData.IsConfirmed = true;

		data = macData;
	} else if ((MHDR & MHDR_MTYPE_MASK) == MHDR_JOIN_REQUEST) {
		if (payloadSize < 23)
			return;

		pt = JoinRequestType;
		JoinRequest joinRequestData;

		memcpy(joinRequestData.AppEUI, payload.data() + 1, 8);
		memcpy(joinRequestData.DevEUI, payload.data() + 9, 8);

		joinRequestData.DevNonce[0] = payload[10];
		joinRequestData.DevNonce[1] = payload[11];

		data = joinRequestData;
	}

	MIC[0] = payload[payloadSize - 4];
	MIC[1] = payload[payloadSize - 3];
	MIC[2] = payload[payloadSize - 2];
	MIC[3] = payload[payloadSize - 1];
}

NSUtils::PHYPayload::PHYPayload(PHYPayload &obj)
{
	*this = obj;
}

NSUtils::PHYPayload &NSUtils::PHYPayload::operator=(PHYPayload &obj)
{
	MHDR = obj.MHDR;
	memcpy(MIC, obj.MIC, 4);

	pt = obj.pt;

	switch (pt) {
	case JoinRequestType:
		data = std::get<JoinRequest>(obj.data);
		break;
	case JoinAcceptType:
		data = std::get<JoinAccept>(obj.data);
		break;
	case MACPayloadType:
		data = std::get<MACPayload>(obj.data);
		break;
	default:
		break;
	}

	return *this;
}

std::vector<uint8_t> NSUtils::PHYPayload::Assemble(void)
{
	std::vector<uint8_t> assembled;

	if (pt == JoinAcceptType) {
		JoinAccept joinAccept = std::get<JoinAccept>(data);

		assembled.push_back(MHDR);

		assembled.insert(assembled.end(), joinAccept.AppNonce, joinAccept.AppNonce + 3);
		assembled.insert(assembled.end(), joinAccept.NetID, joinAccept.NetID + 3);
		assembled.insert(assembled.end(), joinAccept.DevAddr, joinAccept.DevAddr + 4);
		assembled.push_back(joinAccept.DLSettings);
		assembled.push_back(joinAccept.RXDelay);
		assembled.insert(assembled.end(), joinAccept.CFList.begin(), joinAccept.CFList.end());

		assembled.insert(assembled.end(), MIC, MIC + 4);
	} else if (pt == MACPayloadType) {
		MACPayload macPayload = std::get<MACPayload>(data);

		macPayload.FCtrl |= macPayload.FOpt.size() & FCTRL_FOPT_SIZE_MASK;

		assembled.push_back(MHDR);
		assembled.insert(assembled.end(), macPayload.DevAddr, macPayload.DevAddr + 4);
		assembled.push_back(macPayload.FCtrl);
		assembled.push_back(macPayload.FCnt[0]);
		assembled.push_back(macPayload.FCnt[1]);
		assembled.insert(assembled.end(), macPayload.FOpt.begin(), macPayload.FOpt.end());
		assembled.push_back(macPayload.FPort);
		assembled.insert(assembled.end(), macPayload.FRM.begin(), macPayload.FRM.end());
		assembled.insert(assembled.end(), MIC, MIC + 4);
	}

	return assembled;
}

void NSUtils::PHYPayload::CalculateMIC(std::vector<uint8_t> key)
{
	std::vector<uint8_t> mic = NSUtils::CreateMIC(*this, key);

	memcpy(MIC, mic.data(), 4);
}

std::vector<uint8_t> NSUtils::DecodeBase64(std::string encoded, int maxOutLen)
{
	std::vector<uint8_t> raw(maxOutLen, 0);

	if (b64_to_bin(encoded.c_str(), encoded.size(), raw.data(), raw.size()) < 0)
		return std::vector<uint8_t>();

	return raw;
}


std::string NSUtils::BinaryToHex(std::vector<uint8_t> bin)
{
	std::string encoded;

	encoded.resize(bin.size() * 2);

	unsigned int offset = 0, doffset;

	for (unsigned int i = 0; i < bin.size(); ++i) {
		sprintf(((char *) encoded.c_str()) + offset, "%02hhx%n", bin[i], &doffset);
		offset += doffset;
	}

	transform(encoded.begin(), encoded.end(), encoded.begin(), ::toupper);

	return encoded;
}

std::vector<uint8_t> NSUtils::HexToBinary(std::string hex)
{
	std::vector<uint8_t> decoded;

	decoded.resize(hex.size() / 2);

	unsigned int offset = 0, doffset;

	for (unsigned int i = 0; i < decoded.size(); ++i) {
		sscanf(hex.c_str() + offset, "%2hhx%n", &decoded[i], &doffset);
		offset += doffset;
	}

	return decoded;
}

std::vector<uint8_t> NSUtils::CMAC(std::vector<uint8_t> buffer, std::vector<uint8_t> key)
{
	if (key.size() < 16)
		return std::vector<uint8_t>();

    CryptoPP::CMAC<CryptoPP::AES> cmac(key.data(), CryptoPP::AES::MIN_KEYLENGTH);

    std::string mac;
    std::string plain(buffer.begin(), buffer.end());

    CryptoPP::StringSource ss(plain, true,
        new CryptoPP::HashFilter(cmac,
            new CryptoPP::StringSink(mac)
        )
    );

	return std::vector<uint8_t>(mac.begin(), mac.end());
}

std::vector<uint8_t> NSUtils::EncryptCBC(std::vector<uint8_t> buffer, std::vector<uint8_t> key)
{
	std::vector<uint8_t> encryptBuffer;
	uint8_t iv[16];

	if (key.size() < 16)
		return encryptBuffer;

	memset(iv, 0, 16);

	std::string cipher;

	CryptoPP::AES::Encryption encrypter(key.data(), 16);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncrypter(encrypter, iv);
	CryptoPP::StreamTransformationFilter stfEncrypter(cbcEncrypter, new CryptoPP::StringSink(cipher));

	stfEncrypter.Put(buffer.data(), buffer.size());
	stfEncrypter.MessageEnd();

	encryptBuffer.resize(cipher.size());

	memcpy(encryptBuffer.data(), cipher.data(), cipher.size());

	return encryptBuffer;
}

std::vector<uint8_t> NSUtils::EncryptEBC(std::vector<uint8_t> buffer, std::vector<uint8_t> key)
{
	if (key.size() < 16)
		return std::vector<uint8_t>();

	std::string encryptBuffer;
	std::string strbuffer(buffer.begin(), buffer.end());

	CryptoPP::ECB_Mode<CryptoPP::Rijndael>::Encryption encryption(key.data(), 16);
	CryptoPP::StringSource encryptor(strbuffer, true, new CryptoPP::StreamTransformationFilter(encryption, new CryptoPP::StringSink(encryptBuffer), CryptoPP::StreamTransformationFilter::NO_PADDING));

	return std::vector<uint8_t>(encryptBuffer.begin(), encryptBuffer.end());
}

std::vector<uint8_t> NSUtils::DecryptEBC(std::vector<uint8_t> buffer, std::vector<uint8_t> key)
{
	if (key.size() < 16)
		return std::vector<uint8_t>();

	std::string decryptBuffer;
	std::string strbuffer(buffer.begin(), buffer.end());

	CryptoPP::ECB_Mode<CryptoPP::Rijndael>::Decryption decryption(key.data(), 16);
	CryptoPP::StringSource encryptor(strbuffer, true, new CryptoPP::StreamTransformationFilter(decryption, new CryptoPP::StringSink(decryptBuffer), CryptoPP::StreamTransformationFilter::NO_PADDING));

	return std::vector<uint8_t>(decryptBuffer.begin(), decryptBuffer.end());
}

std::vector<uint8_t> NSUtils::DecryptMACPayload(NSUtils::PHYPayload payload, std::vector<uint8_t> key)
{
	std::vector<uint8_t> plainBlock(16);
	MACPayload macData = std::get<MACPayload>(payload.data);
	size_t frmSize = macData.FRM.size();

	plainBlock[0] = 0x01;
	plainBlock[1] = 0x00;
	plainBlock[2] = 0x00;
	plainBlock[3] = 0x00;
	plainBlock[4] = 0x00;
	plainBlock[5] = 0x00; //Dir
	plainBlock[6] = macData.DevAddr[0];
	plainBlock[7] = macData.DevAddr[1];
	plainBlock[8] = macData.DevAddr[2];
	plainBlock[9] = macData.DevAddr[3];
	plainBlock[10] = macData.FCnt[0];
	plainBlock[11] = macData.FCnt[1];
	plainBlock[12] = macData.FCnt[2];
	plainBlock[13] = macData.FCnt[3];
	plainBlock[14] = 0x00;

	size_t blockCount = (size_t) ceil(frmSize / 16.0);
	size_t chunkSize;

	std::vector<uint8_t> encryptedBlock;
	std::vector<uint8_t> decyptedPayload = macData.FRM;

	for (size_t i = 0; i < blockCount; ++i) {
		plainBlock[15] = i + 1;

		encryptedBlock = NSUtils::EncryptEBC(plainBlock, key);

		chunkSize = (i + 1) * 16 < frmSize ? 16 : (frmSize - i * 16);

		for (size_t j = 0; j < chunkSize; ++j)
			decyptedPayload[i * 16 + j] ^= encryptedBlock[j];
	}

	return decyptedPayload;
}

bool NSUtils::VerifyMIC(NSUtils::PHYPayload payload, std::vector<uint8_t> key)
{
	std::vector<uint8_t> mic = CreateMIC(payload, key);

	return mic[0] == payload.MIC[0] &&
			mic[1] == payload.MIC[1] &&
			mic[2] == payload.MIC[2] &&
			mic[3] == payload.MIC[3];
}

std::vector<uint8_t> NSUtils::CreateMIC(PHYPayload payload, std::vector<uint8_t> key)
{
	std::vector<uint8_t> cmacBlock;
	std::vector<uint8_t> mic;

	if (payload.pt == MACPayloadType) {
		MACPayload macData = std::get<MACPayload>(payload.data);

		uint8_t macType = payload.MHDR & MHDR_MTYPE_MASK;

		cmacBlock.resize(16);
		cmacBlock[0] = 0x49;
		cmacBlock[1] = 0x00;
		cmacBlock[2] = 0x00;
		cmacBlock[3] = 0x00;
		cmacBlock[4] = 0x00;

		if (macType == MHDR_CONFIRMED_UP || macType == MHDR_UNCONFIRMED_UP)
			cmacBlock[5] = 0x00;
		else
			cmacBlock[5] = 0x01;

		cmacBlock[6] = macData.DevAddr[0];
		cmacBlock[7] = macData.DevAddr[1];
		cmacBlock[8] = macData.DevAddr[2];
		cmacBlock[9] = macData.DevAddr[3];
		cmacBlock[10] = macData.FCnt[0];
		cmacBlock[11] = macData.FCnt[1];
		cmacBlock[12] = macData.FCnt[2];
		cmacBlock[13] = macData.FCnt[3];
		cmacBlock[14] = 0x00;

		cmacBlock.push_back(payload.MHDR);
		cmacBlock.insert(cmacBlock.end(), macData.DevAddr, macData.DevAddr + 4);
		cmacBlock.push_back(macData.FCtrl);
		cmacBlock.insert(cmacBlock.end(), macData.FCnt, macData.FCnt + 2);
		cmacBlock.insert(cmacBlock.end(), macData.FOpt.begin(), macData.FOpt.end());
		cmacBlock.push_back(macData.FPort);
		cmacBlock.insert(cmacBlock.end(), macData.FRM.begin(), macData.FRM.end());

		cmacBlock[15] = cmacBlock.size() - 16;
	} else if (payload.pt == JoinRequestType) {
		JoinRequest joinRequestData = std::get<JoinRequest>(payload.data);

		cmacBlock.push_back(payload.MHDR);
		cmacBlock.insert(cmacBlock.end(), joinRequestData.AppEUI, joinRequestData.AppEUI + 8);
		cmacBlock.insert(cmacBlock.end(), joinRequestData.DevEUI, joinRequestData.DevEUI + 8);
		cmacBlock.insert(cmacBlock.end(), joinRequestData.DevNonce, joinRequestData.DevNonce + 2);
	} else if (payload.pt == JoinAcceptType) {
		JoinAccept joinAcceptData = std::get<JoinAccept>(payload.data);

		cmacBlock.push_back(payload.MHDR);
		cmacBlock.insert(cmacBlock.end(), joinAcceptData.AppNonce, joinAcceptData.AppNonce + 3);
		cmacBlock.insert(cmacBlock.end(), joinAcceptData.NetID, joinAcceptData.NetID + 3);
		cmacBlock.insert(cmacBlock.end(), joinAcceptData.DevAddr, joinAcceptData.DevAddr + 4);
		cmacBlock.push_back(joinAcceptData.DLSettings);
		cmacBlock.push_back(joinAcceptData.RXDelay);
		cmacBlock.insert(cmacBlock.end(), joinAcceptData.CFList.begin(), joinAcceptData.CFList.end());
	}

	mic = CMAC(cmacBlock, key);

	return mic;
}

uint16_t NSUtils::GenerateRandom16(void)
{
	uint32_t number = GenerateRandom32();

	return (number & 0xFF) ^ (((number >> 8) & 0xFF)) ^ (((number >> 16) & 0xFF) << 8) ^ (((number >> 24) & 0xFF) << 8);
}

uint32_t NSUtils::GenerateRandom32(void)
{
	std::mt19937 generator(std::chrono::high_resolution_clock::now().time_since_epoch().count());

	return generator();
}

std::string NSUtils::AddTime(std::string dateTime, unsigned long seconds)
{
	tm tmDate;
	time_t rawTime;

	strptime(dateTime.c_str(), "%Y-%m-%dT%H:%M:%S", &tmDate);
	rawTime = mktime(&tmDate);
	rawTime += seconds;
	localtime_r(&rawTime, &tmDate);

	strftime((char *) dateTime.c_str(), dateTime.size(), "%Y-%m-%dT%H:%M:%S", &tmDate);

	return dateTime;
}

std::vector<std::string> NSUtils::TokenizeString(std::string str, std::string delim, bool keepEmpty)
{
	std::vector<std::string> result;

	if (delim.empty()) {
		result.push_back(str);

		return result;
	}

	std::string::const_iterator substart = str.begin();
	std::string::const_iterator subend;
	std::string::const_iterator strend = str.end();

	std::function<bool(char)> searcher = [delim](char c) -> bool { return delim.find(c) != std::string::npos; };

	while (true) {
		subend = std::find_if(substart, strend, searcher);
		std::string temp(substart, subend);

		if (keepEmpty || !temp.empty())
			result.push_back(temp);

		if (subend == strend)
			break;

		substart = subend + 1;
	}

	return result;
}
