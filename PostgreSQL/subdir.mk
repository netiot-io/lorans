
PSQL_CPP_SRCS += \
PostgreSQL/Query.cpp \
PostgreSQL/PostgresConnection.cpp

PSQL_OBJS += \
PostgreSQL/Query.o \
PostgreSQL/PostgresConnection.o

PSQL_CPP_DEPS += \
PostgreSQL/Query.d \
PostgreSQL/PostgresConnection.d

PSQL_INCL = \
-IParsers \
-IProcessInterface


# Each subdirectory must supply rules for building sources it contributes
PostgreSQL/%.o: PostgreSQL/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CC) $(STD) $(CCCALLFLAGS) $(PSQL_INCL) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



