/*
 * PostgresConnection.cpp
 *
 *  Created on: Jun 28, 2018
 *      Author: say10
 */

#include "PostgresConnection.hpp"
#include "Query.hpp"
#include <sstream>
#include <iostream>

PostgresConnection::PostgresConnection(void) {}

PostgresConnection::~PostgresConnection(void)
{
	EndDBConnection();
}

void PostgresConnection::EndDBConnection(void)
{
	if (pgconnection != NULL) {
		PQfinish(pgconnection);
		pgconnection = NULL;
		std::cerr << "\nEnded connection to database!\n";
	}
}


void PostgresConnection::InitDBConnection(void)
{
	std::stringstream connectStr;

	const char *env = NULL;

	if ((env = getenv("DATABASE_USER")) != NULL)
		connectStr << "user=" << std::string(env);

	if ((env = getenv("DATABASE_PASSWORD")) != NULL)
		connectStr << " password=" << std::string(env);

	if ((env = getenv("DATABASE_NAME")) != NULL)
		connectStr << " dbname=" << std::string(env);

	if ((env = getenv("DATABASE_ADDRESS")) != NULL)
		connectStr << " host=" << std::string(env);

	if ((env = getenv("DATABASE_PORT")) != NULL)
		connectStr << " port=" << std::string(env);

	std::cerr << "Setting up connection to database:\n\t" + connectStr.str();

	pgconnection = PQconnectdb(connectStr.str().c_str());

	if (pgconnection == NULL || PQstatus(pgconnection) != CONNECTION_OK) {
		std::cerr << "\nFailed to connect to database!\n";

		if (pgconnection != NULL) {
			PQfinish(pgconnection);
			pgconnection = NULL;
		}
	} else
		std::cerr << "\nSuccessfully connected to database!\n";
}


