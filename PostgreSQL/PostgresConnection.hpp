/*
 * PostgresConnection.hpp
 *
 *  Created on: Jun 28, 2018
 *      Author: say10
 */

#ifndef POSTGRESCONNECTION_HPP_
#define POSTGRESCONNECTION_HPP_

#include <libpq-fe.h>

class PostgresConnection {
public:
	PGconn *pgconnection = NULL;

	PostgresConnection(void);
	~PostgresConnection(void);

	void InitDBConnection(void);
	void EndDBConnection(void);
};



#endif /* PostgresConnection_HPP_ */
