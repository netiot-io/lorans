/*
 * Query.cpp
 *
 *  Created on: Mar 24, 2018
 *      Author: Say10
 */

#include "Query.hpp"
#include <sstream>

Query::Query(std::string table)
{
	this->table = table;
}

Query::~Query(void) {}

void Query::AddColumnValue(std::string column, const char *value)
{
	columnValues[column] = '\'' + std::string(value) + '\'';
}

void Query::AddColumnValue(std::string column, std::string value)
{
	columnValues[column] = '\'' + value + '\'';
}

void Query::AddColumnValue(std::string column, double value)
{
	columnValues[column] = std::to_string(value);
}

void Query::AddColumnValue(std::string column, bool value)
{
	columnValues[column] = value ? "TRUE" : "FALSE";
}

void Query::AddSelectedColumn(std::string column)
{
	selectedColumns.push_back(column);
}

std::string Query::GetInsertString(void)
{
	std::stringstream ss;

	size_t columnCount = columnValues.size();

	if (columnCount == 0)
		return "";

	std::vector<std::string> columns;
	std::vector<std::string> values;

	for (std::pair<const std::string, std::string> &elem : columnValues) {
		columns.push_back(elem.first);
		values.push_back(elem.second);
	}

	ss << "insert into " << table << " (";

	ss << columns[0];

	for (size_t i = 1; i < columnCount; ++i)
		ss << ", " << columns[i];

	ss << ") values (";

	ss << values[0];

	for (size_t i = 1; i < columnCount; ++i)
		ss << ", " << values[i];

	ss << ')';

	return ss.str();
}

std::string Query::GetUpdateString(void)
{
	std::stringstream ss;

	size_t columnCount = columnValues.size();

	if (columnCount == 0)
		return "";

	std::vector<std::string> columns;
	std::vector<std::string> values;

	for (std::pair<const std::string, std::string> &elem : columnValues) {
		columns.push_back(elem.first);
		values.push_back(elem.second);
	}

	ss << "update " << table << " set (";

	ss << columns[0];

	for (size_t i = 1; i < columnCount; ++i)
		ss << ", " << columns[i];

	ss << ") = (";

	ss << values[0];

	for (size_t i = 1; i < columnCount; ++i)
		ss << ", " << values[i];

	ss << ')';

	if (!condition.empty() && condition != "")
		ss << " where " << condition;

	return ss.str();
}

std::string Query::GetSelectString(void)
{
	std::stringstream ss;

	size_t columnCount = selectedColumns.size();

	if (columnCount == 0) {
		ss << "select * from " << table;

		if (!condition.empty() && condition != "")
			ss << " where " << condition;

		return ss.str();
	}

	ss << "select ";

	ss << selectedColumns[0];

	for (size_t i = 1; i < columnCount; ++i)
		ss << ", " << selectedColumns[i];

	ss << " from " << table;

	if (!condition.empty() && condition != "")
		ss << " where " << condition;

	return ss.str();
}

std::string Query::GetDeleteString(void)
{
	std::stringstream ss;

	ss << "delete from " << table;

	if (!condition.empty() && condition != "")
		ss << " where " << condition;

	return ss.str();
}

void Query::SetCondition(std::string condition)
{
	this->condition = condition;
}


