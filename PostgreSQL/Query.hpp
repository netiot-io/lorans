/*
 * Query.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: Say10
 */

#include <string>
#include <map>
#include <vector>

class Query {
private:
	std::map<std::string, std::string> columnValues;
	std::string condition;
	std::string table;
	std::vector<std::string> selectedColumns;

public:
	Query(std::string table);
	~Query(void);

	void AddColumnValue(std::string column, const char *value);
	void AddColumnValue(std::string column, std::string value);
	void AddColumnValue(std::string column, double value);
	void AddColumnValue(std::string column, bool value);
	void AddSelectedColumn(std::string column);
	void SetCondition(std::string condition);

	std::string GetInsertString(void);
	std::string GetUpdateString(void);
	std::string GetSelectString(void);
	std::string GetDeleteString(void);
};
