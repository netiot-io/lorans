FROM registry.gitlab.com/netiot-upb/nscontainerbase:1.0.0 as builder

WORKDIR /home
RUN mkdir Maker
WORKDIR Maker
COPY . .
RUN make config
RUN make NetworkServer release

FROM registry.gitlab.com/netiot-upb/nscontainerbase:1.0.0
COPY --from=builder /home/Maker/bin/Release/NetworkServer .
RUN chmod a+x NetworkServer
CMD ./NetworkServer



